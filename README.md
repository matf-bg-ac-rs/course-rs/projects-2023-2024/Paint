# Paint
Simuliraćemo program Paint uz dodatne funkcionalnosti.

# Demo snimak aplikacije :video_camera:
[<img src="Paint/resources/images/paint_logo.png" width="400"/>](https://youtu.be/g7GWAKunfOI)

# Developers
- [Stefan Jevtić, 187/2020](https://gitlab.com/StefanJevtic)
- [Filip Jovanović, 336/2018](https://gitlab.com/fjovanovicc)
- [Vladeta Vujačić, 080/2020](https://gitlab.com/VladetaV)
- [Marko Nikitović, 123/2020](https://gitlab.com/shotinform)
- [Kristijan Petronijević, 357/2022](https://gitlab.com/kristijanpetronijevic)
- [Aleksandar Mladenović, 139/2020](https://gitlab.com/alexa.mladen)

# Korišćeni programski jezici
[![c_plus_plus](https://img.shields.io/badge/Language-C%2B%2B-blue)](https://www.cplusplus.com/) 
[![qt6](https://img.shields.io/badge/Framework-Qt6-green)](https://doc.qt.io/qt-6/) 

# Okruženje
[![qtCreator](https://img.shields.io/badge/IDE-Qt_Creator-green)](https://www.qt.io/product/development-tools)

# Preuzimanje projekta:
1. Otvoriti terminal i pozicionirati se u direktorijum gde ce projekat biti preuzet
2. Uneti jednu od narednih komandi: 
    * `git clone git@gitlab.com:matf-bg-ac-rs/course-rs/projects-2023-2024/Paint.git [naziv_projekta_lokalno]` ako se projekat klonira preko **SSH** protokola

    * `git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/Paint.git [naziv_projekta_lokalno]` ako se projekat klonira preko **HTTPS** protokola

# Učitavanje projekta:
* Preko okruženja **Qt Creator**
    1. Otvoriti okruženje za rad Qt Creator
    2. `File -> Open File or Project`
    3. `putanja_do_lokalnog_projekta/Paint/CMakeLists.txt`

* Preko terminala: <br>

    ```
        sudo apt install -y git gcc g++ cmake qtcreator qtbase6-dev  qtmultimedia6-dev
        git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/Paint
        cd Paint 
        mkdir build 
        cd build 
        cmake ../Paint
        make
    ```
    Ukoliko vam se javlja sledeća greška:
    ```
    CMake Error at server/CMakeLists.txt:1 (find_package):
    Could not find a package configuration file provided by "QT" with any of
    the following names:

        Qt6Config.cmake
        qt6-config.cmake
        Qt5Config.cmake
        qt5-config.cmake

    Add the installation prefix of "QT" to CMAKE_PREFIX_PATH or set "QT_DIR" to
    a directory containing one of the above files.  If "QT" provides a separate
    development package or SDK, be sure it has been installed.
    ```
    Neophodno je da u terminalu pokrenete sledeću komandu:
    `export PATH="$PATH:/home/user/Qt/6.6.0/gcc_64/lib/cmake/"`

    Za više informacija o ovoj grešci, kliknite [ovde](https://gitlab.com/matf-bg-ac-rs/course-azrs/MATF-AZRS/-/tree/main/05-cmake?ref_type=heads#%C4%8Deste-gre%C5%A1ke).

    Za pokretanje programa (u build direktorijumu):
./Paint <br>
    Za pokretanje testova (u build direktorijumu):
./tests/tests


# Pokretanje projekta:
1. Pritisnuti dugme `Build` za prevođenje programa u donjem levom uglu okruženja (ili **Ctrl + B**).
2. Pritisnuti dugme `Run` u donjem levom uglu okruženja.


# Pokretanje testova:
1. Pritisnuti dugme `Build` za prevođenje programa  u donjem levom uglu okruženja (ili **Ctrl + B**)
2. Pritisnuti ikonicu monitora iznad dugmeta `Run` na kojoj stoji ime projekta i `Debug`, zatim u sekciji `Run` izabrati `tests`

:warning: Da bi testovi uspešno prošli potrebno je na svaki pop-up prozor sa tekstom `Do you want to restore previous program state?` pritisnuti dugme `Yes`, a za otvaranje prozora za čuvanje datoteke uneti njeno ime u obliku `ime_datiteke.*`.


# Uputstvo za korišćenje :scroll:
Prilikom pokretanja aplikacije izlazi pop-up prozor `Do you want to restore previous program state?`. 
U zavisnosti od pritisnutog dugmeta biće prikazano podrazumevano platno, odnosno prethodno sačuvano stanje aplikacije. 
Podrazumevano stanje aplikacije omogućava crtanje po platnu crnom bojom velicine 8. 
Među opcijama za crtanje na levoj strani aplikacije nalazi se 5 četkica: 
* `Pen` - podrazumevana četkica, 
* `Pencil` - oslabljena četkica (imitacija drvene bojice),
* `Crayon` - imitacija voštane bojice kod koje se okolina boji slabijom nijansom,
* `OilBrush` - imitacija uljane bojice koja prestaje sa radom nakon kratkog crtanja (simulacija pokreta ruke),
* `WaterBrush` - imitacija vodene bojice koja prestaje sa radom nakon kratkog crtanja (simulacija pokreta ruke).

Pored četkica nalaze se 3 alata: 
* `Bucket` - kantica koja boji panel odabranom bojom,
* `ColorPicker` - menja trenutnu boju u boju sa obrađenog piksela,
* `Eraser` koji briše boje piksela, tj. postavlja boju piksela na podrazumevanu.

Takođe, sa leve strane se nalaze još dve ikonice:
* `Del` - brisanje cele konture
* `Slika palete` - otvaranje palete boja.

U donjem delu aplikacije nalaze se dugmići koji predstavljaju:
* 10 boja
* klizač za menjanje veličine četkica (8-50)
* `Add Panel` - dodavanje panela
* `Remove Panel` - uklanjanje panela
* Pravougaonik koji uzima boju od ColorPicker-a
* `Fit Panel` - za skaliranje crteža na platnu u odnosu na veličinu aplikacije
* `Rotate` - rotacija platna
* `Undo`
* `Redo`

Kao i 4 dugmeta za iscrtavanje geometrijskih oblika na platnu: <br>`Pravouganik`, `Krug`, `Trougao` i `Linija`.

Na desnom delu aplikacije nalaze se kreirana platna koja se mogu kreirati pritiskom na dugme `Add Panel` odnosno obrisati pritiskom na dugme `Remove Panel`.
Takođe, moguće je kretati se po platnima pritiskom na odredjeno platno.

U `menu bar-u` se nalaze opcije za otvaranje odnosno čuvanje scene:
* `File -> Open scene`
* `File -> Save scene`

Opcije za menjanje teme su takođe dostupne u `menu bar-u`, kao i dugme za čišćenje ekrana:
* `Menu -> Light theme`
* `Menu -> Dark theme`
* `Menu -> Space Grey theme`
* `Menu -> Lavender theme`
* `Menu -> Clear all`

Prilikom pritiska na :x: aplikacija pre samog završetka nudi opciju čuvanja panela.
