#ifndef BUTTONINFO_H
#define BUTTONINFO_H

#include <iostream>
#include <QString>
#include <QDebug>

enum class ButtonInfo : unsigned char
{
    WATER_BRUSH,
    OIL_BRUSH,
    BRUSH,
    PENCIL,
    BUCKET,
    SAMPLER,
    ERASER,
    RECTANGLE,
    TRIANGLE,
    CIRCLE,
    LINE,
    CRAYON,
    DELETER
};

class ButtonInfoConverter {
public:
    static QString EnumToString(ButtonInfo tool) {
        static std::map<ButtonInfo, QString> toolMap = {
            {ButtonInfo::WATER_BRUSH, "WATER_BRUSH"},
            {ButtonInfo::OIL_BRUSH, "OIL_BRUSH"},
            {ButtonInfo::BRUSH, "BRUSH"},
            {ButtonInfo::PENCIL, "PENCIL"},
            {ButtonInfo::BUCKET, "BUCKET"},
            {ButtonInfo::SAMPLER, "SAMPLER"},
            {ButtonInfo::ERASER, "ERASER"},
            {ButtonInfo::RECTANGLE, "RECTANGLE"},
            {ButtonInfo::TRIANGLE, "TRIANGLE"},
            {ButtonInfo::CIRCLE, "CIRCLE"},
            {ButtonInfo::LINE, "LINE"},
            {ButtonInfo::CRAYON, "CRAYON"},
            {ButtonInfo::DELETER, "DELETER"}
        };

        auto it = toolMap.find(tool);
        if (it != toolMap.end()) {
            return it->second;
        } else {
            return "Unknown Tool";
        }
    }

    static ButtonInfo StringToEnum(const QString& str) {
        static std::map<QString, ButtonInfo> stringToEnumMap = {
            {"WATER_BRUSH", ButtonInfo::WATER_BRUSH},
            {"OIL_BRUSH", ButtonInfo::OIL_BRUSH},
            {"BRUSH", ButtonInfo::BRUSH},
            {"PENCIL", ButtonInfo::PENCIL},
            {"BUCKET", ButtonInfo::BUCKET},
            {"SAMPLER", ButtonInfo::SAMPLER},
            {"ERASER", ButtonInfo::ERASER},
            {"RECTANGLE", ButtonInfo::RECTANGLE},
            {"TRIANGLE", ButtonInfo::TRIANGLE},
            {"CIRCLE", ButtonInfo::CIRCLE},
            {"LINE", ButtonInfo::LINE},
            {"CRAYON", ButtonInfo::CRAYON},
            {"DELETER", ButtonInfo::DELETER}
        };

        auto it = stringToEnumMap.find(str);
        if (it != stringToEnumMap.end()) {
            return it->second;
        } else {
            return ButtonInfo::PENCIL; // Možete vratiti neku podrazumevanu vrednost ili obraditi grešku kako želite.
        }
    }
};

#endif // BUTTONINFO_H
