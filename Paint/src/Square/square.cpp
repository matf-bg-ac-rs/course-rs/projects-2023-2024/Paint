// Square.cpp
#include "square.h"
#include <QRect>

Square& Square::getInstance() {
    static Square instance(Qt::black, 8);
    return instance;
}

inline QString Square::Type() const { return "Square"; }

void Square::draw(const QPoint& end, QPixmap& pixmap) {
    pixmap = Square::getPixmap();
    QPoint begin_point = Square::getBeginPoint();

    QPainter painter(&pixmap);
    QPen base_pen = Square::getBasePen();
    painter.setPen(base_pen);

    QRect rect = QRect::span(begin_point, end);
    Rectangle::setX( rect.x() );
    Rectangle::setY( rect.y() );

    painter.setRenderHint(QPainter::Antialiasing);
    painter.drawRect(rect);
}

inline int Square::getSide() const { return side_; }

void Square::setSide(const int& side) {
    // ensure that height and width are equal to side
    side_ = side;
    setWidth(side);
    setHeight(side);
}

std::ostream &operator<<(std::ostream &os, const Square &square) {
    os << square.Type().toStdString() << " at (" << square.getX() << ", " << square.getY()
       << ") with side " << square.getSide();
    return os;
}
