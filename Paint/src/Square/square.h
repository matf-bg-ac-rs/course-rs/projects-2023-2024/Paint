// Square.h
#ifndef SQUARE_H
#define SQUARE_H

#include "../Rectangle/rectangle.h"

class Square : public Rectangle {
public:
    static Square& getInstance();
    QString Type() const override;
    void draw(const QPoint& end, QPixmap& pixmap) override;

    int getSide() const;
    void setSide(const int& side);

private:
    // singleton
    Square(const QColor& color, int size)
        : Rectangle(color, size), side_(20)
    {
        setWidth(20);
        setHeight(20);
    }

    Square(const Square&) = delete;
    Square& operator=(const Square&) = delete;

    int side_;
};

std::ostream &operator<<(std::ostream &os, const Square &square);

#endif // SQUARE_H
