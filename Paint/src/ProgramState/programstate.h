#ifndef PROGRAM_STATE_H
#define PROGRAM_STATE_H

#include <QObject>
#include <QStringList>
#include <QMessageBox>
#include <QSettings>
#include <QFile>
#include <QJsonDocument>
#include <filesystem>
#include "../buttoninfo.h"

namespace fs = std::filesystem;


class AppStateInfo {
public:
    AppStateInfo(
        const QStringList& open_files = QStringList(),
        const ButtonInfo active_tool = ButtonInfo::BRUSH,
        const QColor& active_color = QColor(Qt::black)
        ) : open_files_(open_files), active_tool_(active_tool), active_color_(active_color) {}

    QStringList& openFiles();
    ButtonInfo activeTool() const;
    QColor activeColor() const;


    QVariant toVariant() const;
    void fromQVariant(const QVariant& variant);

    void setActiveColor(const QColor &new_active_color);
    void setActiveTool(ButtonInfo new_active_tool);

private:
    QStringList open_files_;
    ButtonInfo active_tool_;
    QColor active_color_;
};

class ProgramState : public QObject {
    Q_OBJECT

public:
    ProgramState(QObject *parent = nullptr) = delete;
    static void saveState(const AppStateInfo& app_state_info);
    static void loadState(AppStateInfo& app_state_info);

private:
    static QMessageBox::StandardButton showDialog();
};

#endif // PROGRAM_STATE_H
