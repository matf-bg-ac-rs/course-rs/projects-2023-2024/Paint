// ProgramState.cpp
#include "programstate.h"


void ProgramState::saveState(const AppStateInfo &app_state_info)
{
    fs::path file_path = fs::current_path() / "stanje_programa.json";

    QFile file(QString::fromStdString(file_path.string()));

    if (file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        QJsonDocument json_document = QJsonDocument::fromVariant(app_state_info.toVariant());

        qDebug() << "Save state: " << json_document;

        file.write(json_document.toJson());
        file.close();
    }
}

void ProgramState::loadState(AppStateInfo& app_state_info) {


    fs::path file_path = fs::current_path() / "stanje_programa.json";
    QFile file(QString::fromStdString(file_path.string()));

    if (!file.exists())
        return;

    QMessageBox::StandardButton response = showDialog();
    if (response == QMessageBox::Yes) {


        if (file.open(QIODevice::ReadOnly)) {
            QByteArray json_data = file.readAll();
            QJsonDocument json_document = QJsonDocument::fromJson(json_data);

            QVariant app_state_variant = json_document.toVariant();
            app_state_info.fromQVariant(app_state_variant);

            file.close();
        }
        else {
            throw std::runtime_error("Nije moguće otvoriti fajl sa stanjem programa: " + file.errorString().toStdString());
        }
    }

    return;
}

QMessageBox::StandardButton ProgramState::showDialog() {

    QMessageBox::StandardButton response = QMessageBox::question(
        nullptr,
        "Program State",
        "Do you want to restore previous program state?",
            QMessageBox::Yes | QMessageBox::No
        );

    return response;
}

QStringList& AppStateInfo::openFiles()
{
    return open_files_;
}

ButtonInfo AppStateInfo::activeTool() const
{
    return active_tool_;
}

QVariant AppStateInfo::toVariant() const
{
    for (const QString& open : open_files_)
        qDebug() << open;

    QVariantMap map;
    map["OpenedFiles"] = QVariant(open_files_);
    map["ChosenTool"] = QVariant(ButtonInfoConverter::EnumToString(active_tool_));

    QVariantMap active_color_map;
    active_color_map["Red"] = activeColor().red();
    active_color_map["Green"] = activeColor().green();
    active_color_map["Blue"] = activeColor().blue();
    map["ActiveColour"] = QVariant(active_color_map);

    return QVariant(map);
}

void AppStateInfo::fromQVariant(const QVariant &variant)
{
    const auto map = variant.toMap();
    open_files_ = map.value("OpenedFiles").toStringList();
    active_tool_ = ButtonInfoConverter::StringToEnum(map.value("ChosenTool").toString());

    bool valid_r, valid_g, valid_b;
    const auto map_c = map.value("ActiveColour").toMap();
    int r = map_c.value("Red").toInt(&valid_r);
    int g = map_c.value("Green").toInt(&valid_g);
    int b = map_c.value("Blue").toInt(&valid_b);

    if (valid_r && valid_g && valid_b)
        active_color_ = QColor(r,g,b);
}

void AppStateInfo::setActiveColor(const QColor &new_active_color)
{
    active_color_ = new_active_color;
}

void AppStateInfo::setActiveTool(ButtonInfo new_active_tool)
{
    active_tool_ = new_active_tool;
}

QColor AppStateInfo::activeColor() const
{
    return active_color_;
}

