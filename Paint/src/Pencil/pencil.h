#ifndef PENCIL_H
#define PENCIL_H

#include "../Pen/pen.h"
#include "../../tests/MyTestPencil/mytestpencil.h"


class Pencil : public Pen
{
public:
    // getters
    int getLightningFactor() const;

    // setters
    void setColor(QColor new_color) override;

    // methods
    static Pencil& getInstance();

private:
    // fields
    int lightning_factor_;
    QColor paint_color_;

    // for singleton class
    Pencil(const Pencil&) = delete;
    Pencil& operator=(const Pencil&) = delete;

    friend class MyTestPencil;

protected:
    // constructors
    Pencil(QColor color, int size, int radius);
};

#endif // PENCIL_H
