#include "pencil.h"


// constructors
Pencil::Pencil(QColor color, int size, int radius)
    : Pen(color, size, radius)
{
    lightning_factor_ = 80;
    paint_color_ = lighterColor(color, lightning_factor_);
    base_pen.setColor(paint_color_);
}


// getters
int Pencil::getLightningFactor() const {
    return lightning_factor_;
}


// setters
void Pencil::setColor(QColor new_color) {
    color = new_color;
    paint_color_ = lighterColor(color, lightning_factor_);
    base_pen.setColor(paint_color_);
}


// methods
Pencil& Pencil::getInstance() {
    static Pencil instance(Qt::black, 8, 0);
    return instance;
}
