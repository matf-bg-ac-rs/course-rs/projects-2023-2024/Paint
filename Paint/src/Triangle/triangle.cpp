// Triangle.cpp
#include <QPolygon>
#include <QPoint>

#include "triangle.h"

Triangle& Triangle::getInstance() {
    static Triangle instance(Qt::black, 8);
    return instance;
}

inline QString Triangle::Type() const { return "Triangle"; }

void Triangle::draw(const QPoint& end, QPixmap& pixmap) {
    pixmap = Triangle::getPixmap();
    QPoint begin_point = Triangle::getBeginPoint();
    QPainter painter(&pixmap);
    QPen base_pen = Triangle::getBasePen();
    painter.setPen(base_pen);

    QPolygon triangle;

    int x1 = begin_point.x();
    int x2 = end.x();
    int y1 = begin_point.y();
    int y2 = end.y();

//    if(y2 < y1)
//        qSwap(start, end_point);

    QPoint node2 = QPoint(x2, y1);

    int x3 = qMin(x1, x2) + qAbs(x2 - x1) / 2;
    QPoint node3 = QPoint(x3, y2);

    triangle << begin_point;
    triangle << node2;
    triangle << node3;

    painter.setRenderHint(QPainter::Antialiasing);
    painter.drawPolygon(triangle);
}

inline int Triangle::getBase() const { return base_; }

inline int Triangle::getHeight() const { return height_; }

void Triangle::setBase(const int& base) { base_ = base; }

void Triangle::setHeight(const int& height) { height_ = height; }

std::ostream &operator<<(std::ostream &os, const Triangle &triangle) {
    os << triangle.Type().toStdString() << " at (" << triangle.getX() << ", " << triangle.getY()
       << ") with base " << triangle.getBase() << " and height " << triangle.getHeight();
    return os;
}
