// Triangle.h
#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "../Shape/shape.h"

class Triangle : public Shape {
public:
    static Triangle& getInstance();
    QString Type() const override;
    void draw(const QPoint& end, QPixmap& pixmap) override;

    // getters
    int getBase() const;
    int getHeight() const;

    // setters
    void setBase(const int&);
    void setHeight(const int&);

private:
    // singleton
    Triangle(const QColor& color, int size)
        : Shape(color, size), base_(20), height_(20)
    {}

    Triangle(const Triangle&) = delete;
    Triangle& operator=(const Triangle&) = delete;

    int base_;
    int height_;
};

std::ostream &operator<<(std::ostream &os, const Triangle &triangle);

#endif // TRIANGLE_H
