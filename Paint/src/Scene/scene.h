#ifndef SCENE_H
#define SCENE_H

#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include <QColorDialog>
#include "../../forms/ui_mainwindow.h"
#include "../Panel/panel.h"
#include "../buttoninfo.h"
#include "../PanelButton/panelbutton.h"
#include <filesystem>
#include <QFileDialog>

#define MAX_PANELS 15

class ExtendedGraphicsScene : public QGraphicsScene
{
    Q_OBJECT

public:
    using QGraphicsScene::QGraphicsScene;

signals:
    void mousePaintAreaClicked(QGraphicsSceneMouseEvent *event);
    void mousePaintAreaMoved(QGraphicsSceneMouseEvent *event);
    void mousePaintAreaReleased(QGraphicsSceneMouseEvent *event);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override
    {
        emit mousePaintAreaClicked(event);
    }

    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override
    {
        emit mousePaintAreaMoved(event);
    }

    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override
    {
        emit mousePaintAreaReleased(event);
    }
};


class Scene : public QMainWindow
{
    Q_OBJECT
public:
    //static Scene& return_unique();

    Ui::MainWindow *ui() const;
    Panel* currentPanel_;
    void change_pixi_item(QPixmap& pixmap);
    Panel* get_current_panel() const;
    Scene(QWidget *parent);
    Scene();
    int panel_n();
    void changeTheme(Panel::Theme new_theme);


    const QVector<Panel *> &panels() const;

public slots:
    void c_color(QColor);
    void c_act(ButtonInfo);
    void c_undo();
    void c_redo();
    void c_dim(int);
    void change_panel(int);
    void add_panel();
    void delete_panel();
    void fit_panel();
    void lavender_theme();
    void dark_theme();
    void space_theme();
    void light_theme();
    void clear_all();
    void rotate_btn_clicked();
    void open_palette_window();
    void save_panel(const QString &file_path);
    void open_image(const QString &file_path);
    void save_panel_empty();
    void open_image_empty();
    void help();

signals:
    void mouse_clicked(QGraphicsSceneMouseEvent *event, Panel& cur_panel);
    void mouse_moved(QGraphicsSceneMouseEvent *event, Panel& cur_panel);
    void mouse_released(QGraphicsSceneMouseEvent *event, Panel& cur_panel);
    void send_color(QColor);
    void send_act(ButtonInfo);
    void send_dim(int);
    void new_panel(Panel*);
    void picked_color(QRgb picked_color);
    void program_closing();

private:

    void showPanel();
    void buttonsSetup();
    void closeEvent(QCloseEvent *event) override;
    QPoint scaleToWindowSize();

    QVector<Panel*> panels_;
    unsigned i_panel_;
    Ui::MainWindow* ui_;
    ExtendedGraphicsScene* paintArea_;
    QGraphicsPixmapItem* pixiItem_;


    Scene( const Scene& ) = delete;
    Scene& operator=( const Scene& ) = delete;

};


#endif // SCENE_H
