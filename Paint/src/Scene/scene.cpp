#include "scene.h"
#include <QDebug>
#include <QLabel>
#include <QUrl>
#include <QDesktopServices>
#include "../Eraser/eraser.h"

// avoiding magic numbers
#define Lavender qRgb(191, 148, 228)
#define Dark qRgb(37,37,38)
#define SpaceGrey qRgb(79,91,102)
#define Light qRgb(255,255,255)

Scene::Scene() : QMainWindow(nullptr),
    ui_(new Ui::MainWindow),
    paintArea_(new ExtendedGraphicsScene())
{
    ui_->setupUi(this);
    showPanel();
    show();

    QSize size = ui_->gvPanel->viewport()->size();
    int width = size.width();
    int height = size.height();

    currentPanel_ = new Panel(width, height);
    pixiItem_ = new QGraphicsPixmapItem(*(currentPanel_->pixmap));
    panels_.push_back(currentPanel_);
    i_panel_ = 0;
    panels_[i_panel_]->setCurrent(true);
    emit new_panel(currentPanel_);
    paintArea_->addItem(pixiItem_);
    ui_->vrt_panels->setAlignment(Qt::AlignTop);

    buttonsSetup();
}


Scene::Scene(QWidget *parent)
    : QMainWindow(parent),
    ui_(new Ui::MainWindow),
    paintArea_(new ExtendedGraphicsScene())
{
    ui_->setupUi(this);
    showPanel();
    show();

    // dimenzije vidljivog ekrana
    QSize size = ui_->gvPanel->viewport()->size();
    int width = size.width();
    int height = size.height();

    currentPanel_ = new Panel(width, height);
    pixiItem_ = new QGraphicsPixmapItem(*(currentPanel_->pixmap));
    panels_.push_back(currentPanel_);
    i_panel_ = 0;
    panels_[i_panel_]->setCurrent(true);
    emit new_panel(currentPanel_);

    paintArea_->addItem(pixiItem_);
    ui_->vrt_panels->setAlignment(Qt::AlignTop);

    buttonsSetup();

    connect(paintArea_, &ExtendedGraphicsScene::mousePaintAreaClicked, this, [this](QGraphicsSceneMouseEvent *event) {
        emit mouse_clicked(event, *currentPanel_);
    });

    connect(paintArea_, &ExtendedGraphicsScene::mousePaintAreaMoved, this, [this](QGraphicsSceneMouseEvent *event) {
        emit mouse_moved(event, *currentPanel_);
    });

    connect(paintArea_, &ExtendedGraphicsScene::mousePaintAreaReleased, this, [this](QGraphicsSceneMouseEvent *event) {
        emit mouse_released(event, *currentPanel_);
    });


    connect(ui_->btn_palette, &QAbstractButton::clicked, this, &Scene::open_palette_window);
    connect(ui_->btn_redo,&QAbstractButton::clicked,this,&Scene::c_redo);
    connect(ui_->btn_undo,&QAbstractButton::clicked,this,&Scene::c_undo);
    connect(ui_->btn_remove_panel,&QAbstractButton::clicked,this,&Scene::delete_panel);
    connect(ui_->btn_add_panel,&QAbstractButton::clicked,this,&Scene::add_panel);
    connect(ui_->sld_dim,&QAbstractSlider::valueChanged,this,&Scene::c_dim);
    connect(ui_->btn_rotate, &QAbstractButton::clicked, this, &Scene::rotate_btn_clicked);
    connect(ui_->btn_fit, &QAbstractButton::clicked, this, &Scene::fit_panel);
    connect(ui_->actionLight_theme, &QAction::triggered, this, &Scene::light_theme);
    connect(ui_->actionDark_theme, &QAction::triggered, this, &Scene::dark_theme);
    connect(ui_->actionSpace_Grey_theme, &QAction::triggered, this, &Scene::space_theme);
    connect(ui_->actionLavender_theme, &QAction::triggered, this, &Scene::lavender_theme);
    connect(ui_->actionClear_all, &QAction::triggered, this, &Scene::clear_all);
    connect(ui_->actionSave_scene, &QAction::triggered, this, &Scene::save_panel_empty);
    connect(ui_->actionOpen, &QAction::triggered, this, &Scene::open_image_empty);
    connect(ui_->actionHelp, &QAction::triggered, this, &Scene::help);

    //REFACTOR
    for (auto btn : ColorButton::colorButtons) {
        QObject::connect(btn, &ColorButton::color_ready, this, &Scene::c_color);
    }
    for (auto btn : ActiveButton::actButtons) {
        btn->make_code();
        QObject::connect(btn, &ActiveButton::activated, this, &Scene::c_act);
    }
    
    // show();
}
void Scene::c_undo(){
    currentPanel_->undo();
}
void Scene::c_redo(){
    currentPanel_->redo();
}
void Scene::c_dim(int info){
    ui_->lbl_dim->setText(QString::number(info));//OVDE
    emit send_dim(info);
}
int Scene::panel_n(){return panels_.size();}
const QVector<Panel *>& Scene::panels() const
{
    return panels_;
}

void Scene::changeTheme(Panel::Theme new_theme){
    QBitmap mask;
    Panel::Theme current_theme = currentPanel_->theme;
    QPainter painter = QPainter(currentPanel_->pixmap);

    if(current_theme == new_theme){
        return;
    }

    switch(current_theme){
    case Panel::Theme::LAVENDER :
        mask = currentPanel_->pixmap->createMaskFromColor(Lavender, Qt::MaskOutColor);
        break;
    case Panel::Theme::DARK :
        mask = currentPanel_->pixmap->createMaskFromColor(Dark, Qt::MaskOutColor);
        break;
    case Panel::Theme::SPACE :
        mask = currentPanel_->pixmap->createMaskFromColor(SpaceGrey, Qt::MaskOutColor);
        break;
    case Panel::Theme::LIGHT :
        mask = currentPanel_->pixmap->createMaskFromColor(Light, Qt::MaskOutColor);
    }

    switch(new_theme){
    case Panel::Theme::LAVENDER :
        painter.setPen(Lavender);
        Eraser::getInstance().setColor(Lavender);
        break;
    case Panel::Theme::DARK :
        painter.setPen(Dark);
        Eraser::getInstance().setColor(Dark);
        break;
    case Panel::Theme::SPACE :
        painter.setPen(SpaceGrey);
        Eraser::getInstance().setColor(SpaceGrey);
        break;
    case Panel::Theme::LIGHT :
        painter.setPen(Light);
        Eraser::getInstance().setColor(Light);
        break;
    }

    painter.drawPixmap(currentPanel_->pixmap->rect(), mask, mask.rect());
    painter.end();

    currentPanel_->state->resetState();
    currentPanel_->onChange();
    change_pixi_item(*currentPanel_->pixmap);

    currentPanel_->theme = new_theme;
}

void Scene::light_theme(){
    changeTheme(Panel::Theme::LIGHT);
}

void Scene::lavender_theme(){
    changeTheme(Panel::Theme::LAVENDER);
}

void Scene::dark_theme(){
    changeTheme(Panel::Theme::DARK);
}

void Scene::space_theme(){
    changeTheme(Panel::Theme::SPACE);
}

void Scene::help(){
    QUrl qurl("https://www.youtube.com/watch?v=g7GWAKunfOI");
    qDebug() << "pre QDesktopServices::openUrl metoda";
    QDesktopServices::openUrl(qurl);
    qDebug() << "posle QDesktopServices::openUrl metoda";
}

void Scene::clear_all(){
    currentPanel_->pixmap->fill(Light);
    currentPanel_->theme = Panel::Theme::LIGHT;
    Eraser::getInstance().setColor(Light);
    currentPanel_->state->resetState();
    currentPanel_->onChange();
    change_pixi_item(*currentPanel_->pixmap);
}

void Scene::c_color(QColor boja){
    emit send_color(boja);
}

void Scene::c_act(ButtonInfo info){
    emit send_act(info);
}

void Scene::change_panel(int index) {
    if (index < panels_.size()){
        currentPanel_ = panels_[index];
        change_pixi_item(*(currentPanel_->pixmap));

        panels_[i_panel_]->setCurrent(false);
        i_panel_ = index;
        panels_[i_panel_]->setCurrent(true);
    }

    switch(currentPanel_->theme){
    case Panel::Theme::LIGHT :
        Eraser::getInstance().setColor(Light);
        break;
    case Panel::Theme::DARK :
        Eraser::getInstance().setColor(Dark);
        break;
    case Panel::Theme::SPACE :
        Eraser::getInstance().setColor(SpaceGrey);
        break;
    case Panel::Theme::LAVENDER :
        Eraser::getInstance().setColor(Lavender);
        break;
    }
}

void Scene::add_panel(){
    if (panels_.size() < MAX_PANELS){
        Panel* help = new Panel();
        emit new_panel(help);
        panels_.push_back(help);
        panels_[i_panel_]->setCurrent(false);
        change_panel(panels_.size() - 1);
        fit_panel();
        QString name = help->getName();
        PanelButton* my_button = new PanelButton(name, i_panel_);
        QObject::connect(my_button, &PanelButton::c_index, this, &Scene::change_panel);
        ui_->vrt_panels->addWidget(my_button);
    }
}

void clearLayout(QVBoxLayout* box) {
    QLayoutItem* item;
    while ((item = box->takeAt(0)) != nullptr) {
        QWidget* help = item->widget();
        box->removeWidget(help);
        delete help;
        delete item;
    }
}
void Scene::buttonsSetup(){
    clearLayout(ui_->vrt_panels);
    int i = 0;
    for (auto panel : panels_){
        QString name = panel->getName();
        PanelButton* my_button = new PanelButton(name,i);
        QObject::connect(my_button, &PanelButton::c_index, this, &Scene::change_panel);
        ui_->vrt_panels->addWidget(my_button);
        i++;
    }
}

void Scene::delete_panel(){
    if(panels_.size() > 1){
        Panel* help = panels_[i_panel_];
        panels_.remove(i_panel_);

        if(i_panel_ != 0)
        {
            i_panel_ = i_panel_ - 1;
        }

        change_panel(i_panel_);
        panels_[i_panel_]->setCurrent(true);
        delete help;
        buttonsSetup();
    }
}

QPoint Scene::scaleToWindowSize() {
    // dimenzije vidljivog ekrana
    QSize size = ui_->gvPanel->viewport()->size();
    int width = size.width();
    int height = size.height();

    qDebug() << "*****************";
    qDebug() << "* width = " << width << " *";
    qDebug() << "* height = " << height << "*";
    qDebug() << "*****************";

    // skalira trenutnu pixmapu na vidljivi ekran
    currentPanel_->scalePixmap(width, height);

    return QPoint(width, height);
}

void Scene::fit_panel() {
    Scene::scaleToWindowSize();
    Scene::change_pixi_item(*currentPanel_->pixmap);
    currentPanel_->onChange();
}

void Scene::rotate_btn_clicked() {

    QPoint size = Scene::scaleToWindowSize();
    int width = size.x();
    int height = size.y();

    int center_x = width / 2;
    int center_y = height / 2;

    // rotacija za 90 stepeni sa centrom u sredini platna
    QTransform transform;
    transform.translate(center_x, center_y);
    transform.rotate(90);
    transform.translate(-center_x, -center_y);

    // skalira zarotiranu pixmapu na vidljivi ekran
    QPixmap* rotatedPixmap = new QPixmap(currentPanel_->pixmap->transformed(transform));
    QPixmap* oldPixmap = currentPanel_->pixmap;
    currentPanel_->pixmap = rotatedPixmap;
    delete oldPixmap;
    currentPanel_->scalePixmap(width, height);

    Scene::change_pixi_item(*currentPanel_->pixmap);
    currentPanel_->onChange();
}

void Scene::showPanel() {
    paintArea_->setSceneRect(ui_->gvPanel->rect());
    ui_->gvPanel->setScene(paintArea_);
    ui_->gvPanel->setRenderHint(QPainter::Antialiasing);
    ui_->gvPanel->setAlignment(Qt::AlignTop | Qt::AlignLeft);
}

Ui::MainWindow *Scene::ui() const
{
    return ui_;
}


void Scene::change_pixi_item(QPixmap &pixmap){
    pixiItem_->setPixmap(pixmap);
}

Panel* Scene::get_current_panel() const {
    return currentPanel_;
}



void Scene::open_palette_window() {
    QColor color = QColorDialog::getColor(Qt::white, this, "Pick a Color");
    emit picked_color(color.rgb());
}

void Scene::save_panel_empty() {
    QFileDialog fileDialog(this, tr("Save Panel Image"), "", tr("Images (*.png *.jpg *.jpeg);;All Files (*)"));
    fileDialog.setAcceptMode(QFileDialog::AcceptSave);

    QStringList supportedFormats;
    supportedFormats << "PNG (*.png)" << "JPEG (*.jpg *.jpeg)";

    fileDialog.setNameFilters(supportedFormats);

    if (fileDialog.exec() == QDialog::Accepted) {
        QStringList selectedFiles = fileDialog.selectedFiles();
        if (!selectedFiles.isEmpty()) {

            QString file_path = selectedFiles.first();
            Scene::save_panel(file_path);
        }
    }
}

void Scene::open_image_empty() {
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Image"), "", tr("Images (*.png *.xpm *.jpg *.bmp)"));
    Scene::open_image(fileName);
}


void Scene::save_panel(const QString &file_path) {
    if (!file_path.isEmpty()) {
        currentPanel_->pixmap->toImage().save(file_path);
        qDebug() << "Save panel: " << file_path;
        currentPanel_->setPath(file_path);
    }
}


void Scene::open_image(const QString &file_path) {

    if (panels().size() == MAX_PANELS)
        return;

    if (!file_path.isEmpty()) {
        QImage image(file_path);
        if (!image.isNull()) {
            add_panel();
            currentPanel_->pixmap->convertFromImage(image);
            currentPanel_->setPath(file_path);
            pixiItem_->setPixmap(*(currentPanel_->pixmap));
        } else {
            qDebug() << "Failed to load image.";
        }
    }
}

void Scene::closeEvent(QCloseEvent *event)
{
    emit program_closing();
    QMainWindow::closeEvent(event);
}
