#include "eraser.h"
#include "qpixmap.h"

// constructors
Eraser::Eraser(Mode mode, qint16 radius, QRgb color) : Tool(), mode_(mode), radius_(radius), color_(color) {}


// getters
Eraser::Mode Eraser::getMode() const {
    return mode_;
}

qint16 Eraser::getRadius() const {
    return radius_;
}

QRgb Eraser::getColor() const
{
    return color_;
}


// setters
void Eraser::setMode(Mode mode){
    mode_ = mode;
}


void Eraser::setColor(QRgb color)
{
    color_ = color;
}

void Eraser::setRadius(int new_radius){ radius_ = new_radius;}

void Eraser::eraseObject(QPoint pos, QPixmap* current_pixmap){
    QImage tmp = current_pixmap->toImage();
    QRgb clickedColor = tmp.pixelColor(pos).rgb();
    QStack<QPoint> points;
    int width = tmp.width();
    int height = tmp.height();

    if(clickedColor == color_){
        return;
    }

    points.push(pos);

    while(!points.empty()){
        QPoint p = points.pop();
        if(tmp.pixelColor(p) == clickedColor){
            tmp.setPixelColor(p, color_);

            if (p.x() > 0)
                points.push(QPoint(p.x() - 1, p.y()));
            if (p.x() < width - 1)
                points.push(QPoint(p.x() + 1, p.y()));
            if (p.y() > 0)
                points.push(QPoint(p.x(), p.y() - 1));
            if (p.y() < height - 1)
                points.push(QPoint(p.x(), p.y() + 1));
        }
    }
    *current_pixmap = QPixmap::fromImage(tmp);
}

void Eraser::erasePoint(QPoint pos, QPixmap* current_pixmap){
    QImage tmp = current_pixmap->toImage();
    int width = tmp.width();
    int height = tmp.height();
    int x_coordinate_to_be_erased;
    int y_coordinate_to_be_erased;
    // erasing every point within radius
    for(int i=-radius_;i<=radius_;i++){
        for(int j=-radius_;j<=radius_;j++){
            x_coordinate_to_be_erased = pos.x() + i;
            y_coordinate_to_be_erased = pos.y() + j;
            if(x_coordinate_to_be_erased >=0 && x_coordinate_to_be_erased<width && y_coordinate_to_be_erased>=0 && y_coordinate_to_be_erased<height){
                tmp.setPixel(QPoint(x_coordinate_to_be_erased,y_coordinate_to_be_erased), color_);
            }
        }
    }
    *current_pixmap = QPixmap::fromImage(tmp);
}

void Eraser::performAction(QPoint position, QPixmap* current_pixmap)
{
    if(this->mode_ == Eraser::Mode::NORMAL){
        this->erasePoint(position, current_pixmap);
    }else{
        this->eraseObject(position, current_pixmap);
    }
}

