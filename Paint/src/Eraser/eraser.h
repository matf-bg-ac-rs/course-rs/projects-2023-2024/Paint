#ifndef ERASER_H
#define ERASER_H

#include "../Tool/tool.h"
#include "qpoint.h"
#include <QRgb>
#include <QStack>

class Eraser: public Tool{


public:
    enum Mode{
        NORMAL,
        DELETING
    };

    static Eraser& getInstance(){
        static Eraser instance = Eraser();
        return instance;
    }

    // getters
    Mode getMode() const;
    qint16 getRadius() const;
    QRgb getColor() const;

    // setters
    void setMode(Mode mode);
    void setRadius(int new_radius);
    void setColor(QRgb color);


    void performAction(QPoint position, QPixmap* current_pixmap) override;




private:
    // constructors
    Eraser(Mode mode = Eraser::Mode::NORMAL, qint16 radius = 10, QRgb color = qRgb(255,255,255));

    // deleted methods
    Eraser(const Eraser&) = delete;
    Eraser& operator=(const Eraser&) = delete;

    void eraseObject(QPoint pos, QPixmap* current_pixmap);
    void erasePoint(QPoint pos, QPixmap* current_pixmap);

    // members
    qint16 radius_;
    Mode mode_;
    QRgb color_;
};

#endif // ERASER_H
