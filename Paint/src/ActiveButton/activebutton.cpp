#include "activebutton.h"

std::map<QString,ButtonInfo> stringToEnum={
    {"WaterBrush",ButtonInfo::WATER_BRUSH},
    {"Pencil",ButtonInfo::PENCIL},
    {"OilBrush",ButtonInfo::OIL_BRUSH},
    {"Crayon",ButtonInfo::CRAYON},
    {"Bucket",ButtonInfo::BUCKET},
    {"Eraser",ButtonInfo::ERASER},
    {"Line",ButtonInfo::LINE},
    {"Circle",ButtonInfo::CIRCLE},
    {"Rect",ButtonInfo::RECTANGLE},
    {"Triangle",ButtonInfo::TRIANGLE},
    {"Sampler",ButtonInfo::SAMPLER},
    {"Brush",ButtonInfo::BRUSH},
    {"Deleter",ButtonInfo::DELETER}
    };

QVector<ActiveButton*> ActiveButton::actButtons;

ActiveButton::ActiveButton(QWidget* parent) : QPushButton(parent) {
    ActiveButton::actButtons.push_back(this);
    QObject::connect(this,&QAbstractButton::clicked,this,&ActiveButton::activate);
}

void ActiveButton::make_code()
{
    QString vrednost;
    auto kod = this->property("code");
    if (kod.isValid()) {
        vrednost = kod.toString();
    }
    this->info_ = stringToEnum[vrednost];
}


void ActiveButton::activate(){
    this->swipe_background();
    ButtonInfo kod = this->getInfo();
    emit activated(kod);
}

void extractBorderImagePath(QString &borderImageString)
{
    int startIndex = borderImageString.indexOf("url(\"");

    if (startIndex != -1) {
        // Pomeri indeks na kraj "url(\""
        startIndex += 5;

        int endIndex = borderImageString.indexOf("\"", startIndex);

        if (endIndex != -1) {
            borderImageString = borderImageString.mid(startIndex, endIndex - startIndex);
            return;
        }
    }

    qDebug() << "Putanja u border-image nije pronađena.";
}

void ActiveButton::swipe_background()
{
    QString originalStyleSheet = this->styleSheet();
    QString borderImageString = this->styleSheet();

    extractBorderImagePath(borderImageString);

    QString newStyleSheet = "QPushButton {"
                            "background-color: rgb(170, 0, 0);"
                            "border-image: url(" + borderImageString + ") 0 0 0 0 stretch stretch;"
                            "border: none;"
                            "background-repeat: no-repeat;"
                            "}";
    this->setStyleSheet(newStyleSheet);

    QTimer::singleShot(100, this, [this, originalStyleSheet]() {
        this->setStyleSheet(originalStyleSheet);
    });
}

