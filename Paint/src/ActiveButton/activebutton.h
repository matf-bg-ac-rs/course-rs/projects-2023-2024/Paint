#ifndef ACTIVEBUTTON_H
#define ACTIVEBUTTON_H

#include <QObject>
#include <QPushButton>
#include <QTimer>
#include <string>
#include<map>
#include<iostream>
#include "../buttoninfo.h"
#include <QVariant>

class ActiveButton : public QPushButton
{
    Q_OBJECT
public:
    static QVector<ActiveButton*> actButtons;
    ActiveButton(QWidget*);
    ButtonInfo getInfo(){
        return info_;
    }

    void setInfo(ButtonInfo info){
        info_ = info;
    }

    void make_code();

public slots:
    void activate();

signals:
    void activated(ButtonInfo);

private:
    ButtonInfo info_;

    void swipe_background();

};
#endif // ACTIVEBUTTON_H
