#include "colorbutton.h"

QVector<ColorButton*> ColorButton::colorButtons;
ColorButton::ColorButton(QWidget* parent) : QPushButton(parent) {
    ColorButton::colorButtons.push_back(this);
    QObject::connect(this,&QAbstractButton::clicked,this,&ColorButton::color_init);
}


void ColorButton::color_init(){
    QColor boja = this->returnColor();
    emit color_ready(boja);
}

QColor ColorButton::returnColor()
{
    QPalette pal = this->palette();
    QColor bgColor = pal.color(QPalette::Button);
    return bgColor;
}

void ColorButton::setButtonColor(const QColor& color) {
    QPalette pal = palette();
    pal.setColor(QPalette::Button, color);
    setPalette(pal);
}

