#ifndef COLORBUTTON_H
#define COLORBUTTON_H

#include <QPushButton>
#include <QWidget>
#include <QObject>
#include <QStyle>
#include <QStyleOptionButton>

class ColorButton : public QPushButton
{
    Q_OBJECT

public:
    ColorButton(QWidget*);
    static QVector<ColorButton*> colorButtons;
    QColor returnColor();
    void setButtonColor(const QColor& color);

public slots:
    void color_init();

signals:
    void color_ready(QColor boja);


};

#endif // COLORBUTTON_H
