#ifndef BUCKET_H
#define BUCKET_H

#include "../Tool/tool.h"
#include "qcolor.h"
#include <QStack>

class Bucket: public Tool{

public:

    static Bucket& getInstance(){
        static Bucket instance = Bucket();
        return instance;
    }
    // getters
    QRgb getColor() const;

    // setters
    void setColor(QRgb color);

    // methods for coloring area - TODO

    void performAction(QPoint position, QPixmap* current_pixmap) override;



private:
    // constructors
    Bucket(QRgb color = qRgb(255, 0, 0));

    // deleted methods
    Bucket(const Bucket&) = delete;
    Bucket& operator=(const Bucket&) = delete;

    void colorArea(QPoint pos, QPixmap* current_pixmap);

    // members
    QRgb color_;
};

#endif // BUCKET_H
