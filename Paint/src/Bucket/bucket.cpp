#include "bucket.h"
#include "qpixmap.h"

// constructors
Bucket::Bucket(QRgb color) : Tool(), color_(color) {}


// getters
QRgb Bucket::getColor() const
{
    return color_;
}

// setters
void Bucket::setColor(QRgb color)
{
    color_ = color;
}

void Bucket::colorArea(QPoint pos, QPixmap* current_pixmap){
    QImage tmp = current_pixmap->toImage();
    QRgb clickedColor = tmp.pixelColor(pos).rgb();
    if(color_ == clickedColor){
        return;
    }
    QStack<QPoint> points;
    int width = tmp.width();
    int height = tmp.height();
    QVector<QVector<bool>> matrica(height, QVector<bool>(width,false));


    points.push(pos);

    while(!points.empty()){
        QPoint p = points.pop();
        if(tmp.pixelColor(p).rgb() == clickedColor){
            tmp.setPixelColor(p, color_);
            if(matrica[p.y()][p.x()] == true){
                continue;
            }else{
                matrica[p.y()][p.x()] = true;
            }

            if (p.x() > 0)
                points.push(QPoint(p.x() - 1, p.y()));
            if (p.x() < width - 1)
                points.push(QPoint(p.x() + 1, p.y()));
            if (p.y() > 0)
                points.push(QPoint(p.x(), p.y() - 1));
            if (p.y() < height - 1)
                points.push(QPoint(p.x(), p.y() + 1));
        }
    }
    *current_pixmap = QPixmap::fromImage(tmp);

}

void Bucket::performAction(QPoint position, QPixmap* current_pixmap)
{
    this->colorArea(position, current_pixmap);
}

