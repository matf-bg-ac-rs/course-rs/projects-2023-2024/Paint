// Rectangle.cpp
#include "rectangle.h"
#include <QRect>

Rectangle& Rectangle::getInstance() {
    static Rectangle instance(Qt::black, 8);
    return instance;
}

inline QString Rectangle::Type() const { return "Rectangle"; }

void Rectangle::draw(const QPoint& end, QPixmap& pixmap) {

    pixmap = Rectangle::getPixmap();
    QPoint begin_point = Rectangle::getBeginPoint();

    QPainter painter(&pixmap);
    QPen base_pen = Rectangle::getBasePen();
    painter.setPen(base_pen);

    QRect rect = QRect::span(begin_point, end);
    Rectangle::setX( rect.x() );
    Rectangle::setY( rect.y() );

    painter.setRenderHint(QPainter::Antialiasing);
    painter.drawRect(rect);
}

int Rectangle::getWidth() const { return width_; }

int Rectangle::getHeight() const { return height_; }

void Rectangle::setWidth(const int& width) { width_ = width; }

void Rectangle::setHeight(const int& height) { height_ = height; }


std::ostream &operator<<(std::ostream &os, const Rectangle &rectangle) {
    os << rectangle.Type().toStdString() << " at (" << rectangle.getX() << ", " << rectangle.getY()
       << ") with width " << rectangle.getWidth() << " and height " << rectangle.getHeight();
    return os;
}
