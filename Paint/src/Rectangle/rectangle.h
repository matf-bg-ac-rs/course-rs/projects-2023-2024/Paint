// Rectangle.h
#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "../Shape/shape.h"

class Rectangle : public Shape {
public:
    static Rectangle& getInstance();
    QString Type() const override;
    void draw(const QPoint& end, QPixmap& pixmap) override;

    // getters
    int getWidth() const;
    int getHeight() const;

    // setters
    void setWidth(const int& width);
    void setHeight(const int& height);

protected:
    // singleton
    Rectangle(const QColor& color, int size)
        : Shape(color, size), width_(30), height_(20)
    {}

    Rectangle(const Rectangle&) = delete;
    Rectangle& operator=(const Rectangle&) = delete;

    int width_;
    int height_;
};

std::ostream &operator<<(std::ostream &os, const Rectangle &rectangle);

#endif // RECTANGLE_H
