// line.cpp
#include "line.h"

Line& Line::getInstance() {
    static Line instance(Qt::black, 8);
    return instance;
}

inline QString Line::Type() const { return "Line"; }

void Line::draw(const QPoint& end, QPixmap& pixmap) {
    pixmap = Line::getPixmap();
    QPoint begin_point = Line::getBeginPoint();
    QPen base_pen = Line::getBasePen();

    QPainter painter(&pixmap);
    painter.setPen(base_pen);
    painter.drawLine(begin_point, end);
}

QPoint Line::getStart() const {
    return start_;
}

QPoint Line::getEnd() const {
    return end_;
}

void Line::setStart(const QPoint& start) {
    start_ = start;
}

void Line::setEnd(const QPoint& end) {
    end_ = end;
}
