// line.h
#ifndef LINE_H
#define LINE_H

#include "../Shape/shape.h"

class Line : public Shape {
public:
    static Line& getInstance();
    QString Type() const override;
    void draw(const QPoint& end, QPixmap& pixmap);

    // getters
    QPoint getStart() const;
    QPoint getEnd() const;

    // setters
    void setStart(const QPoint& start);
    void setEnd(const QPoint& end);

private:
    Line(const QColor& color, int size)
        : Shape(color, size), start_(0, 0), end_(0, 0)
    {}

    Line(const Line&) = delete;
    Line& operator=(const Line&) = delete;

    QPoint start_;
    QPoint end_;
};

#endif // LINE_H
