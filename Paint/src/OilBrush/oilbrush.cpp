#include "oilbrush.h"


// constructors
OilBrush::OilBrush(QColor color, int size, int radius, int distance)
    : Brush(color, size, radius, distance)
{
    new_distance = distance;
    base_lighter_color = lighterColor(color, 20);
    base_pen.setColor(base_lighter_color);
    base_pen.setWidth(size);

    radius_lighter_color = lighterColor(color, 80);
    radius_lighter_color.setAlpha(30);
    radius_pen.setColor(radius_lighter_color);
    radius_pen.setWidth(size + radius);
}


// setters
void OilBrush::setColor(QColor new_color) {
    color = new_color;

    base_lighter_color = lighterColor(color, 20);
    base_pen.setColor(base_lighter_color);

    radius_lighter_color = lighterColor(color, 80);
    radius_lighter_color.setAlpha(30);
    radius_pen.setColor(radius_lighter_color);
}


// methods
OilBrush& OilBrush::getInstance() {
    static OilBrush instance(Qt::black, 8, 4, 550);
    return instance;
}

void OilBrush::draw(const QPoint point) {
    if(painter == nullptr)
        return;

    if(last_position == QPoint(0, 0)) {
        painter->setPen(base_pen);
        painter->drawPoint(point);
        last_position = point;
    }

    QPoint line_length_point = point - last_position;
    int line_length = line_length_point.manhattanLength();

    new_distance -= line_length;

    if(new_distance <= 0) {
        return;
    }

    painter->setPen(radius_pen);
    painter->drawLine(last_position, point);

    painter->setPen(base_pen);
    painter->drawLine(last_position, point);

    last_position = point;
}
