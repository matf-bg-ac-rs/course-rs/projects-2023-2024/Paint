#ifndef OILBRUSH_H
#define OILBRUSH_H

#include "../Brush/brush.h"
#include "../../tests/MyTestOilBrush/mytestoilbrush.h"


class OilBrush : public Brush
{
public:
    // setters
    void setColor(QColor new_color) override;

    // methods
    static OilBrush& getInstance();
    void draw(const QPoint point) override;

private:
    // for singleton class
    OilBrush(const OilBrush&) = delete;
    OilBrush& operator=(const OilBrush&) = delete;

protected:
    // constructors
    OilBrush(QColor color, int size, int radius, int distance);

    friend class MyTestOilBrush;
};

#endif // OILBRUSH_H
