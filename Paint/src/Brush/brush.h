#ifndef BRUSH_H
#define BRUSH_H

#include "../Pen/pen.h"


class Brush : public Pen
{
public:
    // getters
    int getDistance() const;

    // setters
    void setSize(int new_size) override;

    // methods
    void startDrawing(QPixmap& pixmap, const QPoint point) override;
    virtual void draw(const QPoint point) = 0;
    void stoppedDrawing() override;

private:
    using Pen::getInstance;

    // for singleton class
    Brush(const Brush&) = delete;
    Brush& operator=(const Brush&) = delete;

protected:
    // constructors
    Brush(QColor color, int size, int radius, int distance);

    // fields
    int distance;
    int new_distance;
    QColor base_lighter_color;
    QColor radius_lighter_color;
    QPen radius_pen;
};

#endif // BRUSH_H
