#include "brush.h"


// constructors
Brush::Brush(QColor color, int size, int radius, int distance)
    : Pen(color, size, radius), distance(distance)
{
    base_pen.setCapStyle(Qt::SquareCap);
    base_pen.setJoinStyle(Qt::BevelJoin);
}


// getters
int Brush::getDistance() const {
    return distance;
}


// setters
void Brush::setSize(int new_size) {
    size = new_size;

    base_pen.setWidth(size);
    radius_pen.setWidth(size + radius);
}


// methods
void Brush::startDrawing(QPixmap &pixmap, const QPoint point) {
    painter = new QPainter(&pixmap);
    draw(point);
}

void Brush::stoppedDrawing() {
    last_position = QPoint(0, 0);
    new_distance = distance;

    if(painter != nullptr) {
        delete painter;
        painter = nullptr;
    }
}
