#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsLineItem>
#include <QMouseEvent>
#include <QTimer>
#include<QObject>
#include "../../forms/ui_mainwindow.h"
#include "../Scene/scene.h"
#include "../Pen/pen.h"
#include "../Shape/shape.h"
#include "../Tool/tool.h"
#include "../OilBrush/oilbrush.h"
#include "../WaterBrush/waterbrush.h"
#include "../Pencil/pencil.h"
#include "../Crayon/crayon.h"
#include "../Bucket/bucket.h"
#include "../Eraser/eraser.h"
#include "../ColorPicker/colorpicker.h"
#include "../Rectangle/rectangle.h"
#include "../Line/line.h"
#include "../Triangle/triangle.h"
#include "../Circle/circle.h"
#include "../ProgramState/programstate.h"
#include <QMessageBox>


class QGraphicsScene;
class Panel;

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);

    Scene *scene() const;
    void sceneClose();

    enum Active {
        TOOL,PEN,SHAPE
    };

    QColor active_color_;
    int active_dimension_;


    Active activeInteractor() const;
    Pen *activePen() const;
    bool getSamplerActive() const;

    Tool *activeTool() const;

public slots:
    void color(QColor);
    void activator(ButtonInfo);
    void mouse_clicked_action(QGraphicsSceneMouseEvent *event, Panel& cur_panel);
    void mouse_moved_action(QGraphicsSceneMouseEvent *event, Panel& cur_panel);
    void mouse_released_action(QGraphicsSceneMouseEvent *event, Panel& cur_panel);
    void dimension(int);
    void connect_to(Panel* panel);
    void modify_color_dialog(QRgb color);
    void process_save();

protected:
    void processLoad();


public slots:
    void pix_draw();

private:
    template <typename T>
    void updateInteractor(Active active, T* interactor) {
        active_interactor_ = active;

        if(active_color_ != interactor->getColor())
            interactor->setColor(active_color_);

        if(active_dimension_ != interactor->getSize())
            interactor->setSize(active_dimension_);
    }

    void updatePen();
    void updateShape();
    void panelSaving(Panel* panel);
    QMessageBox::StandardButton saveQuery();

    Scene* scene_;
    Active active_interactor_;
    Pen* active_pen_;
    Shape* active_shape_;
    Tool* active_tool_;
    bool sampler_active_;
    AppStateInfo app_state_;
};
#endif
