#include "mainwindow.h"
#include <iostream>
#include <QDebug>
#include <QPalette>

#include "../Pencil/pencil.h"
#include "../Brush/brush.h"
#include "../Crayon/crayon.h"
#include "../OilBrush/oilbrush.h"
#include "../WaterBrush/waterbrush.h"
#include "../ProgramState/programstate.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    scene_(new Scene(nullptr)),
    sampler_active_(false)
{


    //_active_interactor = PEN;
    connect(scene_, &Scene::send_color, this, &MainWindow::color);
    connect(scene_, &Scene::send_act, this, &MainWindow::activator);
    connect(scene_, &Scene::mouse_clicked, this, &MainWindow::mouse_clicked_action);
    connect(scene_, &Scene::mouse_moved, this, &MainWindow::mouse_moved_action);
    connect(scene_, &Scene::mouse_released, this, &MainWindow::mouse_released_action);
    connect(scene_,&Scene::new_panel,this,&MainWindow::connect_to);
    connect(scene_->get_current_panel(),&Panel::change,this,&MainWindow::pix_draw);
    connect(scene_,&Scene::send_dim,this,&MainWindow::dimension);
    connect(&ColorPicker::getInstance(), &ColorPicker::sampleTaken, this, &MainWindow::modify_color_dialog);
    connect(scene_, &Scene::picked_color, this, &MainWindow::modify_color_dialog);
    connect(scene_, &Scene::program_closing, this,  &MainWindow::process_save);


    active_pen_ = &(Pen::getInstance());
    active_shape_ = &(Rectangle::getInstance());

    active_dimension_ = 8;

    processLoad();
}

void MainWindow::connect_to(Panel* panel){
    connect(panel, &Panel::change, this, &MainWindow::pix_draw);
}

void MainWindow::modify_color_dialog(QRgb color)
{
    QString styleSheet = QString("background-color: %1; color: white;").arg(QColor(color).name());
    scene_->ui()->btn_ccolor->setStyleSheet(styleSheet);
}

void MainWindow::pix_draw(){
    scene_->change_pixi_item(*(scene_->get_current_panel()->pixmap));
}

Tool *MainWindow::activeTool() const
{
    return active_tool_;
}

bool MainWindow::getSamplerActive() const
{
    return sampler_active_;
}

MainWindow::Active MainWindow::activeInteractor() const
{
    return active_interactor_;
}

Pen *MainWindow::activePen() const
{
    return active_pen_;
}

void MainWindow::processLoad()
{
    try {
        ProgramState::loadState(app_state_);
    }
    catch (const std::runtime_error& e) {
        qDebug() << "Uhvaćen izuzetak: " << e.what();
    }

    QStringList files_to_open = app_state_.openFiles();

    for (QString& file : files_to_open)
        scene_->open_image(file);

    active_color_ = app_state_.activeColor();
    activator(app_state_.activeTool());

}

void MainWindow::process_save()
{
    qDebug() << "process_save";
    const QVector<Panel*>& panels = scene_->panels();
    app_state_.openFiles().clear();
    qDebug() << app_state_.openFiles().length();

    for (int i = 0; i < panels.size(); ++i){

        Panel* p = panels[i];

        if (!p->changed() && p->path() == "")
            continue;

        scene_->change_panel(i);
        qDebug() <<"Putanja: " << p->path();
        qDebug() << "Is changed" << p->changed();

        if (p->changed()){

            QMessageBox::StandardButton response = saveQuery();
            if (response == QMessageBox::No)
                continue;
            panelSaving(p);
        }

        app_state_.openFiles().push_back(p->path());
    }
    app_state_.setActiveColor(active_color_);
    ProgramState::saveState(app_state_);
    qDebug() << "Proces save, end.";
}

void MainWindow::color(QColor color){
    active_color_ = color;
    Bucket::getInstance().setColor(color.rgb());

    if(active_interactor_ == Active::PEN){
        active_pen_->setColor(color);
    }

    if(active_interactor_ == Active::SHAPE){
        active_shape_->setColor(color);
    }
}

void MainWindow::panelSaving(Panel *panel)
{
    QString path = panel->path();
    if (path != ""){
        scene_->save_panel(path);
    }
    else{
        scene_->save_panel_empty();
    }
}

void MainWindow::dimension(int dim){
    active_dimension_ = dim;
    qDebug()<<"Nova dimezija je"<<dim;
    Eraser::getInstance().setRadius(dim);
    active_pen_->setSize(dim);
    active_shape_->setSize(dim);
}

void MainWindow::activator(ButtonInfo info){
    app_state_.setActiveTool(info);

    switch(info){//Treba da se doda samo _active_pen = singlton obejkat kad ga napravite
        case ButtonInfo::BRUSH :
            active_interactor_=Active::PEN;
            active_pen_ = &(Pen::getInstance());
            updatePen();
            break;
        case ButtonInfo::OIL_BRUSH:
            active_pen_ = &(OilBrush::getInstance());
            updatePen();
            break;
        case ButtonInfo::WATER_BRUSH:
            active_pen_ = &(WaterBrush::getInstance());
            updatePen();
            break;
        case ButtonInfo::PENCIL:
            active_pen_ = &(Pencil::getInstance());
            updatePen();
            break;
        case ButtonInfo::CRAYON:
            active_pen_ = &(Crayon::getInstance());
            updatePen();
            break;
        case ButtonInfo::BUCKET:
            active_interactor_=Active::TOOL;
            active_tool_ = &(Bucket::getInstance());
            sampler_active_ = false;
            break;
        case ButtonInfo::SAMPLER:
            active_interactor_=Active::TOOL;
            active_tool_ = &(ColorPicker::getInstance());
            sampler_active_ = true;
            break;
        case ButtonInfo::ERASER:
            qDebug() << "Postavljena gumica";
            active_interactor_=Active::TOOL;
            active_tool_ = &(Eraser::getInstance());
            Eraser::getInstance().setMode(Eraser::Mode::NORMAL);
            qDebug() << active_tool_;
            sampler_active_ = false;
            break;
        case ButtonInfo::RECTANGLE:
            active_shape_ = &(Rectangle::getInstance());
            updateShape();
            break;
        case ButtonInfo::TRIANGLE:
            active_shape_ = &(Triangle::getInstance());
            updateShape();
            break;
        case ButtonInfo::CIRCLE:
            active_shape_ = &(Circle::getInstance());
            updateShape();
            break;
        case ButtonInfo::LINE:
            active_shape_ = &(Line::getInstance());
            updateShape();
            break;
        case ButtonInfo::DELETER:
            active_interactor_ = Active::TOOL;
            active_tool_ = &(Eraser::getInstance());
            Eraser::getInstance().setMode(Eraser::Mode::DELETING);
            sampler_active_ = false;
            break;
        }
}

void MainWindow::mouse_clicked_action(QGraphicsSceneMouseEvent *event, Panel& cur_panel){
    switch (active_interactor_) {
        case PEN:
            if(event->button() == Qt::LeftButton) {
                active_pen_->startDrawing(*(scene_->get_current_panel()->pixmap), event->scenePos().toPoint());
            }
        case SHAPE:
            active_shape_->start_drawing(event->scenePos().toPoint(), *(cur_panel.pixmap));
            break;

        case TOOL:
            active_tool_->performAction(event->scenePos().toPoint(), scene_->get_current_panel()->pixmap);
            break;
    }

    scene_->change_pixi_item(*(cur_panel.pixmap));
}

void MainWindow::mouse_moved_action(QGraphicsSceneMouseEvent *event, Panel& cur_panel){
    if(event->buttons() == Qt::LeftButton) {
        switch (active_interactor_) {
            case PEN:
                active_pen_->draw(event->scenePos().toPoint());
                break;

            case SHAPE :
                active_shape_->draw(event->scenePos().toPoint(), *(cur_panel.pixmap));
                qDebug() << "Shape moved";
                break;

            case TOOL:
                qDebug() << "Tool moved";
                active_tool_->performAction(event->scenePos().toPoint(), scene_->get_current_panel()->pixmap);
                break;
        }
        scene_->change_pixi_item(*(cur_panel.pixmap));
    }
}

void MainWindow::mouse_released_action(QGraphicsSceneMouseEvent *event, Panel& cur_panel) {
    if(active_interactor_ == Active::PEN) {
        active_pen_->stoppedDrawing();
        scene_->change_pixi_item(*(cur_panel.pixmap));
    }

    if(active_interactor_ == Active::SHAPE)
        active_shape_->draw(event->scenePos().toPoint(), *(cur_panel.pixmap));

    if(!(active_interactor_ == Active::TOOL && sampler_active_ == true))
        scene_->currentPanel_->onChange();
}


void MainWindow::updatePen() {
    updateInteractor(Active::PEN, active_pen_);
}

void MainWindow::updateShape() {
    updateInteractor(Active::SHAPE, active_shape_);
}


QMessageBox::StandardButton MainWindow::saveQuery() {

    QMessageBox::StandardButton response = QMessageBox::question(
        nullptr,
        "Save",
        "Do you want to save panel?",
        QMessageBox::Yes | QMessageBox::No
        );

    return response;
}

// samo za testiranje se koristi
void MainWindow::sceneClose(){
    scene_->close();
}
