#include "states.h"

template <typename T>
States<T>::States()
{

}

template <typename T>
States<T>::States(T* obj) {
    now_ = 0;
    T* help = new T(*obj);
    before_.prepend(help);
}

template <typename T>
States<T>::~States(){
    for (auto elem : before_)
        delete elem;
}

template <typename T>
T* States<T>::undo(){
    if (now_ < before_.size()-1){
        now_++;
        return getActive();
    }
    return nullptr;
}

template <typename T>
T* States<T>::redo(){
    if(now_ > 0){
        now_--;
        return getActive();
    }
    return nullptr;
}

template <typename T>
T* States<T>::getActive(){
    if(before_.size()>0){
        T* help = new T(*before_[now_]);
        return help;
    }
    return nullptr;
}
template <typename T>
void States<T>::onChange(T* obj){
    QVector<T*> help;
    unsigned int n = now_;
    for (int i = n;i>0;i--){
        help.push_back(before_.takeFirst());
    }
    now_ = 0;
    for(auto elem : help)
        delete elem;

    T* temp = new T(*obj);
    if(before_.size() < MAX_DEPTH){
        before_.prepend(temp);
    }else{
        before_.pop_back();
        before_.prepend(temp);
    }

}

template<typename T>
void States<T>::resetState()
{
    while(before_.size() >= 1){
        auto element = before_.takeLast();
        delete element;
    }
    now_ = 0;
}

