#ifndef STATES_H
#define STATES_H

#include <QVector>
#include <QObject>
#include <QPixmap>

#define MAX_DEPTH 10



template<typename T>
class States : public QObject
{

public:
    States();
    States(T*);
    T* undo();
    T* redo();
    ~States();
    T* getActive();
    void onChange(T*);
    void resetState();
    QVector<T*> before_;
private:
    unsigned int now_;

};


template class States<QPixmap>;
template class States<int>;
#endif // STATES_H
