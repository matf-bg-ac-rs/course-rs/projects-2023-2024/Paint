#ifndef TOOL_H
#define TOOL_H


#include <QObject>

class Tool: public QObject {

Q_OBJECT

public:

    // constructors
    Tool();

    // pure virtual methods
    virtual void performAction(QPoint position, QPixmap* current_pixmap) = 0;

private:

};

#endif // TOOL_H
