#ifndef PANEL_H
#define PANEL_H


#include <QColor>
#include <QGraphicsScene>
#include <QPixmap>
#include <QPoint>
#include <QStack>
#include <qwidget.h>
#include <qgraphicsitem.h>
#include <qpixmap.h>
#include "../States/states.h"

class Panel : public QObject {
    Q_OBJECT
public:
    // constructors
    Panel(int width = 1300, int height = 600);

    // getters
    QColor getPointColor(const QPoint& point);
    bool getCurrent();


    QString getName(){return name_;}
    int getId(){return my_id_;}

    // setters
    void setPointColor(const QPoint& point, QColor color);
    void setCurrent(const bool& x);
    void setName(const QString& name);

    static void resetCounter();
    void undo();
    void redo();
    void onChange();
    void scalePixmap(const int& width, const int& height);
    QString path() const;
    void setPath(const QString &newPath);
    bool changed() const;

signals:
    void change();

private:
    static int id_;
    int my_id_;
    bool current_;
    QString name_;
    QString path_;
    bool changed_;

    void set_changed(){changed_ = true;}

public:
    //members
    int width;
    int height;
    States<QPixmap>* state;
    QPixmap* pixmap;
    enum Theme{
        LIGHT,
        DARK,
        SPACE,
        LAVENDER
    };
    Theme theme;
};

#endif // PANEL_H
