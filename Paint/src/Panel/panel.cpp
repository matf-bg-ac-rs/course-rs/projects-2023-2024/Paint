#include "panel.h"

int Panel::id_ = 1;

void Panel::resetCounter(){
    id_=1;
}

Panel::Panel(int width , int height ) : 
    theme(Theme::LIGHT),
    changed_(false),
    path_(""){
    pixmap = new QPixmap(width,height);
    pixmap->fill(Qt::white);
    state = new States<QPixmap>(pixmap);

    my_id_ = id_++;
    name_ = "Panel_" + QString::number(my_id_);
    current_ = false;
}


// getters
QColor Panel::getPointColor(const QPoint& point) {
    QImage tmp = pixmap->toImage();
    QColor color = tmp.pixelColor(point);
    return color;
}


bool Panel::getCurrent() { return current_; }


// setters
void Panel::setPointColor(const QPoint& point, QColor color) {
    QImage tmp = pixmap->toImage();
    tmp.setPixelColor(point, color);

    QPixmap* oldPixmap = pixmap;
    pixmap = new QPixmap(QPixmap::fromImage(tmp));
    delete oldPixmap;
}

inline void Panel::setName(const QString &name) { name_ = name; }

void Panel::setCurrent(const bool &x) { current_ = x; }


void Panel::undo(){

    if(current_){
        QPixmap* help = state->undo();
        if(help != nullptr){
            *pixmap = *help;
            delete help;
            emit change();
        }
    }
}

void Panel::redo(){
    if(current_){
        QPixmap* help = state->redo();
        if(help != nullptr){
            *pixmap = *help;
            delete help;
            emit change();
        }
    }
}

void Panel::onChange(){
    set_changed();
    state->onChange(pixmap);
    QPixmap* help = state->getActive();
    QPixmap* temp = pixmap;
    pixmap = help;
    delete temp;

}

bool Panel::changed() const
{
        return changed_;
}

QString Panel::path() const
{
        return path_;
}

void Panel::setPath(const QString &newPath)
{
        path_ = newPath;
}


void Panel::scalePixmap(const int& width, const int& height) {
    QPixmap* oldPixmap = pixmap;
    pixmap = new QPixmap(pixmap->scaled(width, height));
    delete oldPixmap;
}
