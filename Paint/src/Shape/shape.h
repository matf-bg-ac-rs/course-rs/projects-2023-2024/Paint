#ifndef SHAPE_H
#define SHAPE_H

#include <QColor>
#include <QString>
#include <QPoint>
#include <QPainter>
#include <QPixmap>
#include <QPen>
#include <ostream>

class Shape
{
public:
    Shape(const QColor& color = Qt::black, int size = 8);
    void start_drawing(QPoint begin_point, QPixmap& begin_pixmap);

    virtual ~Shape();

    // getters
    int getX() const;
    int getY() const;
    int getSize() const;
    QColor getColor() const;
    QPixmap getPixmap() const;
    QPoint getBeginPoint() const;
    QPen getBasePen() const;

    // setters
    void setX(const int& x);
    void setY(const int& y);
    void setSize(const int& size);
    void setColor(const QColor& color);

    // pure virtual methods
    virtual QString Type() const = 0;
    virtual void draw(const QPoint& end, QPixmap& pixmap) = 0;
    //virtual Shape *Copy() const = 0;

private:
    int x_, y_;
    int size_;
    QPen base_pen_;
    QColor color_;
    QPoint begin_point_;
    QPixmap begin_pixmap_;
};

std::ostream &operator<<(std::ostream &os, const Shape &shape);

#endif // SHAPE_H
