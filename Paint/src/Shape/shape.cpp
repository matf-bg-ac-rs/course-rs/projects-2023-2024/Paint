// Shape.cpp
#include "shape.h"

Shape::Shape(const QColor& color, int size)
    : color_(color), size_(size)
{
    base_pen_ = QPen(color_, size_, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    begin_point_ = QPoint(0, 0);
}

Shape::~Shape() {}

void Shape::start_drawing(QPoint begin_point, QPixmap& begin_pixmap){
    begin_point_ = begin_point;
    begin_pixmap_ = begin_pixmap;
}

// getters
int Shape::getX() const {
    return x_;
}

int Shape::getY() const {
    return y_;
}

int Shape::getSize() const {
    return size_;
}

QColor Shape::getColor() const {
    return color_;
}

QPixmap Shape::getPixmap() const {
    return begin_pixmap_;
}

QPoint Shape::getBeginPoint() const {
    return begin_point_;
}

QPen Shape::getBasePen() const {
    return base_pen_;
}

// setters
void Shape::setX(const int& x) {
    x_ = x;
}

void Shape::setY(const int& y) {
    y_ = y;
}

void Shape::setSize(const int& size) {
    size_ = size;
    base_pen_.setWidth(size_);
}

void Shape::setColor(const QColor& color) {
    color_ = color;
    base_pen_.setColor(color_);
}

std::ostream &operator<<(std::ostream &os, const Shape &shape) {
    os << shape.Type().toStdString() << " at (" << shape.getX() << ", " << shape.getY() << ")";
    return os;
}
