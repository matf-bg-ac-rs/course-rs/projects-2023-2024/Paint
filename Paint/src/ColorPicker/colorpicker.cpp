#include "colorpicker.h"
#include "qpixmap.h"

// constructors
ColorPicker::ColorPicker(QRgb color) : Tool(), color_(color) {}


// getters
QRgb ColorPicker::getColor() const{
    return color_;
}

// setters
void ColorPicker::setColor(QRgb color) {
    color_ = color;
}

void ColorPicker::takeSample(QPoint position, QPixmap* current_pixmap){
    QImage tmp = current_pixmap->toImage();
    color_ = tmp.pixelColor(position).rgb();
    emit sampleTaken(color_);
}

void ColorPicker::performAction(QPoint position, QPixmap* current_pixmap)
{
    this->takeSample(position, current_pixmap);
}

