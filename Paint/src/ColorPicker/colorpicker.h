#ifndef COLORPICKER_H
#define COLORPICKER_H

#include "../Tool/tool.h"
#include "qpoint.h"
#include <QObject>
#include <QRgb>

class ColorPicker: public Tool{

    Q_OBJECT

public:

    static ColorPicker& getInstance(){
        static ColorPicker instance = ColorPicker();
        return instance;
    }

    // getters
    QRgb getColor() const;

    // setters
    void setColor(QRgb color);


    void performAction(QPoint position, QPixmap* current_pixmap) override;


signals:
    void sampleTaken(QRgb color);


private:

    // constructors
    ColorPicker(QRgb color = Qt::white);
    // deleted methods
    ColorPicker(const ColorPicker&) = delete;
    ColorPicker& operator=(const ColorPicker&) = delete;

    void takeSample(QPoint position, QPixmap* current_pixmap);

    // members
    QRgb color_;
};

#endif // COLORPICKER_H
