// Circle.cpp
#include "circle.h"
#include <QRect>

Circle& Circle::getInstance()
{
    static Circle instance(Qt::black, 8);
    return instance;
}

inline QString Circle::Type() const { return "Circle"; }

void Circle::draw(const QPoint& end, QPixmap& pixmap) {
    pixmap = Circle::getPixmap();
    QPoint begin_point = Circle::getBeginPoint();
    QPainter painter(&pixmap);
    QPen base_pen = Circle::getBasePen();
    painter.setPen(base_pen);

    QRect rect = QRect::span(begin_point, end);
    QPoint center = rect.center();
    Circle::setX( center.x() );
    Circle::setY( center.y() );

    painter.setRenderHint(QPainter::Antialiasing);
    painter.drawEllipse(rect);
}

inline int Circle::getRadius() const { return radius_; }

void Circle::setRadius(const int& radius) { radius_ = radius; }

std::ostream &operator<<(std::ostream &os, const Circle &circle) {
    os << circle.Type().toStdString() << " at (" << circle.getX() << ", " << circle.getY()
       << ") with radius " << circle.getRadius();
    return os;
}
