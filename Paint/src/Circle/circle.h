// Circle.h
#ifndef CIRCLE_H
#define CIRCLE_H

#include "../Shape/shape.h"

class Circle : public Shape {
public:
    static Circle& getInstance();
    QString Type() const override;
    void draw(const QPoint& end, QPixmap& pixmap) override;

    // getters
    int getRadius() const;

    // setters
    void setRadius(const int& radius);

private:
    // singleton
    Circle(const QColor& color, int size)
        : Shape(color, size), radius_(20)
    {}

    Circle(const Circle&) = delete;
    Circle& operator=(const Circle&) = delete;

    int radius_;
};

std::ostream &operator<<(std::ostream &os, const Circle &circle);

#endif // CIRCLE_H
