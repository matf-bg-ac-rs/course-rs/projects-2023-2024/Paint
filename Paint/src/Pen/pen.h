#ifndef PEN_H
#define PEN_H

#include <QPainter>
#include <QPixmap>

#include "../../tests/MyTestPen/mytestpen.h"


class Pen
{
public:
    // getters
    QColor getColor() const;
    int getSize() const;
    int getRadius() const;

    // setters
    virtual void setColor(QColor new_color);
    virtual void setSize(int new_size);

    // methods
    static Pen& getInstance();
    virtual void startDrawing(QPixmap& pixmap, const QPoint point);
    virtual void draw(const QPoint point);
    virtual void stoppedDrawing();

private:
    // for singleton class
    Pen(const Pen&) = delete;
    Pen& operator=(const Pen&) = delete;

protected:
    // constructors
    Pen(QColor color, int size, int radius);

    // fields
    QPainter* painter;
    QColor color;
    int size;
    int radius;
    QPen base_pen;
    QPoint last_position;

    // methods
    QColor lighterColor(const QColor old_color, const int factor);

    friend class MyTestPen;
};

#endif // PEN_H
