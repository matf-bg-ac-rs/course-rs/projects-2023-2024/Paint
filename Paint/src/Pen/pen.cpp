#include "pen.h"

#include <QPainter>


// constructors
Pen::Pen(QColor color, int size, int radius)
    : color(color), size(size), radius(radius)
{
    base_pen = QPen(color, size, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    last_position = QPoint(0, 0);
}


// getters
QColor Pen::getColor() const {
    return color;
}

int Pen::getSize() const {
    return size;
}

int Pen::getRadius() const {
    return radius;
}


// setters
void Pen::setColor(QColor new_color) {
    color = new_color;
    base_pen.setColor(color);
}

void Pen::setSize(int new_size) {
    size = new_size;
    base_pen.setWidth(size);
}


// methods
Pen& Pen::getInstance() {
    static Pen instance(Qt::black, 8, 0);
    return instance;
}

void Pen::startDrawing(QPixmap& pixmap, const QPoint point) {
    painter = new QPainter(&pixmap);
    draw(point);
}

void Pen::draw(const QPoint point) {
    if(painter == nullptr)
        return;

    painter->setPen(base_pen);

    if(last_position == QPoint(0, 0)) {
        painter->drawPoint(point);
    } else {
        painter->drawLine(last_position, point);
    }

    last_position = point;
}

void Pen::stoppedDrawing() {
    last_position = QPoint(0, 0);

    if(painter != nullptr) {
        delete painter;
        painter = nullptr;
    }
}

// Ne mogu da koristim ugradjen metod .lighter() jer crnu ne pretvara u svetliju nijansu crne,
//   a crna boja je default boja za cetkice
// Konvertuje se prvo u HSV koordinate, ali H i S komponente nisu definisane kod crne boje
// https://bugreports.qt.io/browse/QTBUG-9343
QColor Pen::lighterColor(const QColor old_color, const int factor) {
    int new_red = old_color.red() + factor;
    int new_green = old_color.green() + factor;
    int new_blue = old_color.blue() + factor;

    new_red = qBound(0, new_red, 255);
    new_green = qBound(0, new_green, 255);
    new_blue = qBound(0, new_blue, 255);

    QColor new_color(new_red, new_green, new_blue);

    return new_color;
}
