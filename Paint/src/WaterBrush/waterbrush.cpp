#include "waterbrush.h"


// constructors
WaterBrush::WaterBrush(QColor color, int size, int radius, int distance)
    : Brush(color, size, radius, distance)
{
    new_distance = distance;
    base_lighter_color = lighterColor(color, 70);
    base_pen.setColor(base_lighter_color);
    base_pen.setWidth(size);

    radius_lighter_color = lighterColor(color, 180);
    radius_lighter_color.setAlpha(20);
    radius_pen.setColor(radius_lighter_color);
    radius_pen.setWidth(size + radius);
}


// setters
void WaterBrush::setColor(QColor new_color) {
    color = new_color;

    base_lighter_color = lighterColor(color, 70);
    base_pen.setColor(base_lighter_color);

    radius_lighter_color = lighterColor(color, 180);
    radius_lighter_color.setAlpha(20);
    radius_pen.setColor(radius_lighter_color);
}


// methods
WaterBrush& WaterBrush::getInstance() {
    static WaterBrush instance(Qt::black, 8, 8, 400);
    return instance;
}

void WaterBrush::draw(const QPoint point) {
    if(painter == nullptr)
        return;

    if(last_position == QPoint(0, 0)) {
        painter->setPen(base_pen);
        painter->drawPoint(point);
        last_position = point;
    }

    QPoint line_length_point = point - last_position;
    int line_length = line_length_point.manhattanLength();

    new_distance -= line_length;

    if(new_distance <= 0) {
        return;
    }

    painter->setPen(radius_pen);
    painter->drawLine(last_position, point);

    painter->setPen(base_pen);
    painter->drawLine(last_position, point);

    last_position = point;
}
