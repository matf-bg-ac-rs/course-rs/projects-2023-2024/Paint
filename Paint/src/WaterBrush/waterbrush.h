#ifndef WATERBRUSH_H
#define WATERBRUSH_H

#include "../Brush/brush.h"
#include "../../tests/MyTestWaterBrush/mytestwaterbrush.h"


class WaterBrush : public Brush {
public:
    // setters
    void setColor(QColor new_color) override;

    // methods
    static WaterBrush& getInstance();
    void draw(const QPoint point) override;

private:
    // for singleton class
    WaterBrush(const WaterBrush&) = delete;
    WaterBrush& operator=(const WaterBrush&) = delete;

protected:
    // constructors
    WaterBrush(QColor color, int size, int radius, int distance);

    friend class MyTestWaterBrush;
};

#endif // WATERBRUSH_H
