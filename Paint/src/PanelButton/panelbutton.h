#ifndef PANELBUTTON_H
#define PANELBUTTON_H

#include <QObject>
#include <QPushButton>
#include <QWidget>

class PanelButton : public QPushButton
{
    Q_OBJECT
public:
    PanelButton(QWidget*);
    PanelButton(const QString&,int);
    int getIndex(){return index_;}
    void setIndex(int x){index_ = x;}

private:
    int index_;
public slots:
    void get_index();
signals:
    void c_index(int);
};

#endif // PANELBUTTON_H
