#include "panelbutton.h"

PanelButton::PanelButton(QWidget* parent) : QPushButton(parent){}

PanelButton::PanelButton(const QString& name, int index) : QPushButton(name){
    index_ = index;
    QObject::connect(this, &QAbstractButton::clicked, this, &PanelButton::get_index);
}

void PanelButton::get_index(){
    emit c_index(index_);
}

