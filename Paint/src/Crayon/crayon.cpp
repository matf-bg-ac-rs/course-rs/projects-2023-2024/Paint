#include "crayon.h"


// constructors
Crayon::Crayon(QColor color, int size, int radius)
    : Pen(color, size, radius)
{
    radius_pen_ = QPen(color, size + radius);

    base_lighter_color_ = color;
    base_lighter_color_.setAlpha(20);
    base_pen.setColor(base_lighter_color_);

    radius_lighter_color_ = color;
    radius_lighter_color_.setAlpha(30);
    radius_pen_.setColor(radius_lighter_color_);
}


// setters
void Crayon::setColor(QColor new_color) {
    color = new_color;

    base_lighter_color_ = color;
    base_lighter_color_.setAlpha(20);
    base_pen.setColor(base_lighter_color_);

    radius_lighter_color_ = color;
    radius_lighter_color_.setAlpha(30);
    radius_pen_.setColor(radius_lighter_color_);
}

void Crayon::setSize(int new_size) {
    size = new_size;
    base_pen.setWidth(size);
    radius_pen_.setWidth(size + radius);
}


// methods
Crayon& Crayon::getInstance() {
    static Crayon instance(Qt::black, 8, 8);
    return instance;
}

void Crayon::startDrawing(QPixmap& pixmap, const QPoint point) {
    painter = new QPainter(&pixmap);
    draw(point);
}

void Crayon::draw(const QPoint point) {
    if(painter == nullptr)
        return;

    if(last_position == QPoint(0, 0)) {
        last_position = point;
    }

    painter->setPen(base_pen);
    painter->drawLine(last_position, point);

    painter->setPen(radius_pen_);
    painter->drawLine(last_position, point);

    last_position = point;
}
