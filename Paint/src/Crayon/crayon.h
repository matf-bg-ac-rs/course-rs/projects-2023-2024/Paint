#ifndef CRAYON_H
#define CRAYON_H

#include "../Pen/pen.h"
#include "../../tests/MyTestCrayon/mytestcrayon.h"


class Crayon : public Pen
{
public:
    // setters
    void setColor(QColor new_color) override;
    void setSize(int new_size) override;

    // methods
    void startDrawing(QPixmap& pixmap, const QPoint point) override;
    static Crayon& getInstance();
    void draw(const QPoint point) override;

private:
    // fields
    QColor base_lighter_color_;
    QColor radius_lighter_color_;
    QPen radius_pen_;

    // for singleton class
    Crayon(const Crayon&) = delete;
    Crayon& operator=(const Crayon&) = delete;

protected:
    // constructors
    Crayon(QColor color, int size, int radius);

    friend class MyTestCrayon;
};

#endif // CRAYON_H
