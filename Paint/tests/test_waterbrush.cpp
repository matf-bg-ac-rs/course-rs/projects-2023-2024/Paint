#include "catch.hpp"

#include "../src/WaterBrush/waterbrush.h"
#include "MyTestWaterBrush/mytestwaterbrush.h"

#define WATERBRUSH_DISTANCE   400


TEST_CASE("Test case for class WaterBrush", "[WaterBrush]") {
    SECTION("Class initialization and checking default fields") {
        WaterBrush& waterbrush = WaterBrush::getInstance();

        QColor waterbrush_color = waterbrush.getColor();
        int waterbrush_size = waterbrush.getSize();
        int waterbrush_radius = waterbrush.getRadius();
        int waterbrush_distance = waterbrush.getDistance();

        REQUIRE(waterbrush_color == Qt::black);
        REQUIRE(waterbrush_size == 8);
        REQUIRE(waterbrush_radius == 8);
        REQUIRE(waterbrush_distance == WATERBRUSH_DISTANCE);
    }

    SECTION("Same instance of WaterBrush is returned (singleton class") {
        WaterBrush& waterbrush1 = WaterBrush::getInstance();
        WaterBrush& waterbrush2 = WaterBrush::getInstance();

        REQUIRE(&waterbrush1 == &waterbrush2);
    }

    SECTION("Maximum size setter") {
        WaterBrush& waterbrush = WaterBrush::getInstance();

        waterbrush.setSize(50);
        int waterbrush_size = waterbrush.getSize();

        REQUIRE(waterbrush_size == 50);
    }

    SECTION("Minimum size setter") {
        WaterBrush& waterbrush = WaterBrush::getInstance();

        waterbrush.setSize(8);
        int waterbrush_size = waterbrush.getSize();

        REQUIRE(waterbrush_size == 8);
    }

    SECTION("Color setters") {
        WaterBrush& waterbrush = WaterBrush::getInstance();

        waterbrush.setColor(Qt::white);
        QColor white_color = waterbrush.getColor();

        waterbrush.setColor(Qt::red);
        QColor red_color = waterbrush.getColor();

        waterbrush.setColor(Qt::blue);
        QColor blue_color = waterbrush.getColor();

        waterbrush.setColor(QColor(3, 11, 133));
        QColor rgb_color = waterbrush.getColor();

        waterbrush.setColor(Qt::black);
        QColor black_color = waterbrush.getColor();

        REQUIRE(white_color == Qt::white);
        REQUIRE(red_color == Qt::red);
        REQUIRE(blue_color == Qt::blue);
        REQUIRE(rgb_color == QColor(3, 11, 133));
        REQUIRE(black_color == Qt::black);
    }

    SECTION("Default base_pen style, cap and join") {
        MyTestWaterBrush test_waterbrush;

        int style = test_waterbrush.getBasePenStyle();
        int cap = test_waterbrush.getBasePenCap();
        int join = test_waterbrush.getBasePenJoin();

        REQUIRE(style == Qt::SolidLine);
        REQUIRE(cap == Qt::SquareCap);
        REQUIRE(join == Qt::BevelJoin);
    }

    SECTION("Variable new_distance is adjusted based on point provided in the method draw") {
        WaterBrush& waterbrush = WaterBrush::getInstance();
        MyTestWaterBrush test_waterbrush;

        QPoint set_last_position(2, 1);
        test_waterbrush.setLastPosition(set_last_position);

        QPixmap pixmap(600, 600);
        QPoint point(12, 15);
        waterbrush.startDrawing(pixmap, point);

        int old_distance = waterbrush.getDistance();
        waterbrush.draw(point);
        int new_distance = test_waterbrush.getNewDistance();
        waterbrush.stoppedDrawing();

        QPoint new_point = point - set_last_position;
        int new_point_distance = new_point.manhattanLength();
        int expected_new_distance = old_distance - new_point_distance;

        REQUIRE(new_distance == expected_new_distance);
    }
}
