#include "catch.hpp"

#include "../src/Pencil/pencil.h"
#include "MyTestPencil/mytestpencil.h"

#define LIGHTNING_FACTOR    80


TEST_CASE("Test case for class Pencil", "[Pencil]") {
    SECTION("Class initialization and checking default fields") {
        Pencil& pencil = Pencil::getInstance();
        MyTestPencil test_pencil;

        QColor pencil_paint_color = test_pencil.getPaintColor();
        int pencil_size = pencil.getSize();
        int pencil_radius = pencil.getRadius();

        REQUIRE(pencil_paint_color == QColor(LIGHTNING_FACTOR, LIGHTNING_FACTOR, LIGHTNING_FACTOR));
        REQUIRE(pencil_size == 8);
        REQUIRE(pencil_radius == 0);
    }

    SECTION("Same instance of Pencil is returned (singleton class") {
        Pencil& pencil1 = Pencil::getInstance();
        Pencil& pencil2 = Pencil::getInstance();

        REQUIRE(&pencil1 == &pencil2);
    }

    SECTION("Minimum size setter") {
        Pencil& pencil = Pencil::getInstance();

        pencil.setSize(8);
        int pencil_size = pencil.getSize();

        REQUIRE(pencil_size == 8);
    }

    SECTION("Maximum size setter") {
        Pencil& pencil = Pencil::getInstance();

        pencil.setSize(50);
        int pencil_size = pencil.getSize();

        REQUIRE(pencil_size == 50);
        pencil.setSize(8);
    }

    SECTION("Lighter painting color setters") {
        Pencil& pencil = Pencil::getInstance();
        MyTestPencil test_pencil;

        pencil.setColor(Qt::white);
        QColor white_color = test_pencil.getPaintColor();

        pencil.setColor(Qt::red);
        QColor red_color = test_pencil.getPaintColor();

        pencil.setColor(Qt::blue);
        QColor blue_color = test_pencil.getPaintColor();

        int rgb_red = 233;
        int rgb_green = 113;
        int rgb_blue = 89;
        pencil.setColor(QColor(rgb_red, rgb_green, rgb_blue));
        QColor rgb_color = test_pencil.getPaintColor();

        pencil.setColor(Qt::black);
        QColor black_color = test_pencil.getPaintColor();

        int expected_rgb_red = qBound(0, rgb_red + LIGHTNING_FACTOR, 255);
        int expected_rgb_green = qBound(0, rgb_green + LIGHTNING_FACTOR, 255);
        int expected_rgb_blue = qBound(0, rgb_blue + LIGHTNING_FACTOR, 255);

        REQUIRE(white_color == Qt::white);
        REQUIRE(red_color == QColor(255, LIGHTNING_FACTOR, LIGHTNING_FACTOR));
        REQUIRE(blue_color == QColor(LIGHTNING_FACTOR, LIGHTNING_FACTOR, 255));
        REQUIRE(rgb_color == QColor(expected_rgb_red, expected_rgb_green, expected_rgb_blue));
        REQUIRE(black_color == QColor(LIGHTNING_FACTOR, LIGHTNING_FACTOR, LIGHTNING_FACTOR));
    }

    SECTION("Check if point is colored in default lighter black after draw(const QPoint point, QPainter& painter) was called") {
        Pencil& pencil = Pencil::getInstance();

        QPixmap pixmap(1000, 1000);
        QPoint point(10, 10);

        pencil.startDrawing(pixmap, point);
        pencil.draw(point);
        pencil.stoppedDrawing();
        QColor colored_pixel = pixmap.toImage().pixelColor(point);

        REQUIRE(colored_pixel == QColor(LIGHTNING_FACTOR, LIGHTNING_FACTOR, LIGHTNING_FACTOR));
    }
}
