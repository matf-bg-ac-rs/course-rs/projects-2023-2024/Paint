#include "catch.hpp"
#include "../src/Tool/tool.h"
#include "../src/Bucket/bucket.h"
#include <qpixmap.h>

TEST_CASE("Testing bucket class", "[Bucket]"){
    SECTION("Testing if getInstance method returns valid instance"){
        // arrange

        // act
        Bucket& bucket = Bucket::getInstance();

        // assert
        REQUIRE_FALSE(&bucket == nullptr);
    }

    SECTION("Testing if getInstance method returns same instances every time"){
        // arrange

        // act
        Bucket& bucket1 = Bucket::getInstance();
        Bucket& bucket2 = Bucket::getInstance();

        // assert
        REQUIRE(&bucket1 == &bucket2);
    }

    SECTION("For all-white pixmap, bucket colors whole pixmap in same color"){
        // arrange
        QPixmap* pixmap = new QPixmap(1000,1000);
        pixmap->fill(qRgb(255,255,255));
        Bucket& bucket = Bucket::getInstance();
        QRgb expected_color = qRgb(5,5,5);

        // act
        bucket.setColor(qRgb(5,5,5));
        bucket.performAction(QPoint(5,5),pixmap);
        QRgb result_color1 = pixmap->toImage().pixelColor(QPoint(5,5)).rgb();
        QRgb result_color2 = pixmap->toImage().pixelColor(QPoint(250,475)).rgb();
        QRgb result_color3 = pixmap->toImage().pixelColor(QPoint(346,987)).rgb();

        // assert
        REQUIRE(expected_color == result_color1);
        REQUIRE(expected_color == result_color2);
        REQUIRE(expected_color == result_color3);
        delete pixmap;
    }

    SECTION("Testing if bucket sets color properly"){
        // arrange
        Bucket& bucket = Bucket::getInstance();
        QRgb expected_color = qRgb(150,150,150);

        // act
        bucket.setColor(qRgb(150,150,150));
        QRgb result_color = bucket.getColor();

        // assert
        REQUIRE(result_color == expected_color);
    }

    SECTION("Testing if performAction method doesn't throw exception for valid arguments"){
        // arrange
        QPixmap* pixmap = new QPixmap(1000,1000);
        pixmap->fill(qRgb(100,100,100));
        Bucket& bucket = Bucket::getInstance();
        QPoint position(5,5);

        // act/assert
        REQUIRE_NOTHROW(bucket.performAction(position,pixmap));
        delete pixmap;
    }

}
