#include "catch.hpp"
#include "../src/Scene/scene.h"


TEST_CASE("Save and Open Image", "[Scene]") {

    Scene scene;

    QString file_path = "temp_image.png";

    Panel* panel = new Panel();
    panel->pixmap->fill(Qt::red);

    scene.currentPanel_ = panel;
    scene.save_panel(file_path);

    SECTION("Save Panel") {
        QFile file(file_path);
        REQUIRE(file.exists());
    }

    SECTION("Save Image Empty") {
        scene.save_panel_empty();
        QFile file(file_path);
        REQUIRE(file.exists());

    }

    QFile::remove(file_path);
}
