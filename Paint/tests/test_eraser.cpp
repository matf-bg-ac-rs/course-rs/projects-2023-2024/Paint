#include "catch.hpp"
#include "../src/Tool/tool.h"
#include "../src/Eraser/eraser.h"

#include <qpixmap.h>


TEST_CASE("Testing eraser class", "[Eraser]"){

    SECTION("Testing if getInstance method returns valid instance"){
        // arrange

        // act
        Eraser& eraser = Eraser::getInstance();

        // assert
        REQUIRE_FALSE(&eraser == nullptr);
    }

    SECTION("Testing if getInstance method returns same instances every time") {
        // arrange

        // act
        Eraser& eraser1 = Eraser::getInstance();
        Eraser& eraser2 = Eraser::getInstance();

        // assert
        REQUIRE(&eraser1 == &eraser2);
    }

    SECTION("Testing if setMode method works properly for setting NORMAL mode"){
        // arrange
        Eraser& eraser = Eraser::getInstance();
        Eraser::Mode expected_mode = Eraser::Mode::NORMAL;

        // act
        eraser.setMode(Eraser::Mode::NORMAL);
        Eraser::Mode result_mode = eraser.getMode();

        // assert
        REQUIRE(expected_mode == result_mode);
    }


    SECTION("Testing if setMode method works properly for setting DELETING mode"){
        // arrange
        Eraser& eraser = Eraser::getInstance();
        Eraser::Mode expected_mode = Eraser::Mode::DELETING;

        // act
        eraser.setMode(Eraser::Mode::DELETING);
        Eraser::Mode result_mode = eraser.getMode();

        // assert
        REQUIRE(expected_mode == result_mode);
    }

    SECTION("Testing if eraser erases point properly"){
        // arrange
        QPixmap* pixmap = new QPixmap(1000,1000);
        pixmap->fill(qRgb(150,150,150));
        Eraser& eraser = Eraser::getInstance();
        eraser.setMode(Eraser::Mode::NORMAL);
        QPoint position(5,5);
        QRgb expected_color = qRgb(255,255,255);

        // act
        eraser.performAction(position,pixmap);
        QRgb result_color = pixmap->toImage().pixelColor(position).rgb();

        // assert
        REQUIRE(result_color == expected_color);
        delete pixmap;
    }

    SECTION("Testing if erasePoint method doesn't throw exception for valid arguments"){
        // arrange
        QPixmap* pixmap = new QPixmap(1000,1000);
        Eraser& eraser = Eraser::getInstance();
        eraser.setMode(Eraser::Mode::NORMAL);
        QPoint position(5,5);

        // act/assert
        REQUIRE_NOTHROW(eraser.performAction(position,pixmap));
        delete pixmap;
    }


    SECTION("Testing if eraseObject method doesn't throw exception for valid arguments"){
        // arrange
        QPixmap* pixmap = new QPixmap(1000,1000);
        Eraser& eraser = Eraser::getInstance();
        eraser.setMode(Eraser::Mode::DELETING);
        QPoint position(5,5);

        REQUIRE_NOTHROW(eraser.performAction(position,pixmap));
        delete pixmap;
    }


}
