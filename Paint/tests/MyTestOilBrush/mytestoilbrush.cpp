#include "mytestoilbrush.h"


MyTestOilBrush::MyTestOilBrush() {}

int MyTestOilBrush::getNewDistance() {
    OilBrush& oilbrush = OilBrush::getInstance();

    return oilbrush.new_distance;
}

int MyTestOilBrush::getBasePenStyle() {
    OilBrush& oilbrush = OilBrush::getInstance();

    return oilbrush.base_pen.style();
}

int MyTestOilBrush::getBasePenCap() {
    OilBrush& oilbrush = OilBrush::getInstance();

    return oilbrush.base_pen.capStyle();
}

int MyTestOilBrush::getBasePenJoin() {
    OilBrush& oilbrush = OilBrush::getInstance();

    return oilbrush.base_pen.joinStyle();
}

QPoint MyTestOilBrush::getLastPosition() {
    OilBrush& oilbrush = OilBrush::getInstance();

    return oilbrush.last_position;
}

void MyTestOilBrush::setLastPosition(const QPoint& new_last_position) {
    OilBrush& oilbrush = OilBrush::getInstance();

    oilbrush.last_position = new_last_position;
}
