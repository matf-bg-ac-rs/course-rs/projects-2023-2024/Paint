#ifndef MYTESTOILBRUSH_H
#define MYTESTOILBRUSH_H

#include "../../src/OilBrush/oilbrush.h"


class MyTestOilBrush
{
public:
    MyTestOilBrush();

    int getNewDistance();
    int getBasePenStyle();
    int getBasePenCap();
    int getBasePenJoin();
    QPoint getLastPosition();
    void setLastPosition(const QPoint& new_last_position);
};

#endif // MYTESTOILBRUSH_H
