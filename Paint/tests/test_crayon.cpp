#include "catch.hpp"

#include "../src/Crayon/crayon.h"
#include "MyTestCrayon/mytestcrayon.h"

#define CRAYON_BASE_PEN_ALPHA   20
#define CRAYON_RADIUS_PEN_ALPHA 30
#define CRAYON_RADIUS           8


TEST_CASE("Test case for class Crayon", "[Crayon]") {
    SECTION("Class initialization and checking default fields") {
        Crayon& crayon = Crayon::getInstance();

        QColor crayon_color = crayon.getColor();
        int crayon_size = crayon.getSize();
        int crayon_radius = crayon.getRadius();

        REQUIRE(crayon_color == Qt::black);
        REQUIRE(crayon_size == 8);
        REQUIRE(crayon_radius == 8);
    }

    SECTION("Same instance of Crayon is returned (singleton class") {
        Crayon& crayon1 = Crayon::getInstance();
        Crayon& crayon2 = Crayon::getInstance();

        REQUIRE(&crayon1 == &crayon2);
    }

    SECTION("Maximum size setter") {
        Crayon& crayon = Crayon::getInstance();
        MyTestCrayon test_crayon;

        crayon.setSize(50);
        int base_pen_size = crayon.getSize();
        int radius = crayon.getRadius();
        int radius_pen_size = test_crayon.getRadiusPenSize();

        REQUIRE(base_pen_size == 50);
        REQUIRE(radius_pen_size == 50 + radius);
    }

    SECTION("Minimum size setter") {
        Crayon& crayon = Crayon::getInstance();
        MyTestCrayon test_crayon;

        crayon.setSize(8);
        int base_pen_size = crayon.getSize();
        int radius = crayon.getRadius();
        int radius_pen_size = test_crayon.getRadiusPenSize();

        REQUIRE(base_pen_size == 8);
        REQUIRE(radius_pen_size == 8 + radius);
    }

    SECTION("Base pen and radius pen colors are the same (just different alpha parameter)") {
        MyTestCrayon test_crayon;

        QColor base_pen_color = test_crayon.getBaseLighterColor();
        QColor radius_pen_color = test_crayon.getRadiusLighterColor();

        REQUIRE(base_pen_color == radius_pen_color);
    }

    SECTION("Color setters and alpha parameters check") {
        Crayon& crayon = Crayon::getInstance();
        MyTestCrayon test_crayon;

        crayon.setColor(Qt::white);
        QColor white_color = crayon.getColor();

        crayon.setColor(Qt::red);
        QColor red_color = crayon.getColor();

        crayon.setColor(Qt::blue);
        QColor blue_color = crayon.getColor();

        crayon.setColor(QColor(200, 100, 53));
        QColor rgb_color = crayon.getColor();

        crayon.setColor(Qt::black);
        QColor black_color = crayon.getColor();
        int base_lighter_color_alpha = test_crayon.getBaseAlpha();
        int radius_lighter_color_alpha = test_crayon.getRadiusAlpha();

        REQUIRE(white_color == Qt::white);
        REQUIRE(red_color == Qt::red);
        REQUIRE(blue_color == Qt::blue);
        REQUIRE(rgb_color == QColor(200, 100, 53));
        REQUIRE(black_color == Qt::black);
        REQUIRE(base_lighter_color_alpha == CRAYON_BASE_PEN_ALPHA);
        REQUIRE(radius_lighter_color_alpha == CRAYON_RADIUS_PEN_ALPHA);
    }
}
