#include "catch.hpp"

#include "../src/OilBrush/oilbrush.h"
#include "MyTestOilBrush/mytestoilbrush.h"

#define OILBRUSH_DISTANCE   550


TEST_CASE("Test case for class OilBrush", "[OilBrush]") {
    SECTION("Class initialization and checking default fields") {
        OilBrush& oilbrush = OilBrush::getInstance();

        QColor oilbrush_color = oilbrush.getColor();
        int oilbrush_size = oilbrush.getSize();
        int oilbrush_radius = oilbrush.getRadius();
        int oilbrush_distance = oilbrush.getDistance();

        REQUIRE(oilbrush_color == Qt::black);
        REQUIRE(oilbrush_size == 8);
        REQUIRE(oilbrush_radius == 4);
        REQUIRE(oilbrush_distance == 550);
    }

    SECTION("Same instance of OilBrush is returned (singleton class") {
        OilBrush& oilbrush1 = OilBrush::getInstance();
        OilBrush& oilbrush2 = OilBrush::getInstance();

        REQUIRE(&oilbrush1 == &oilbrush2);
    }

    SECTION("Maximum size setter") {
        OilBrush& oilbrush = OilBrush::getInstance();

        oilbrush.setSize(50);
        int oilbrush_size = oilbrush.getSize();

        REQUIRE(oilbrush_size == 50);
    }

    SECTION("Minimum size setter") {
        OilBrush& oilbrush = OilBrush::getInstance();

        oilbrush.setSize(8);
        int oilbrush_size = oilbrush.getSize();

        REQUIRE(oilbrush_size == 8);
    }

    SECTION("Color setters") {
        OilBrush& oilbrush = OilBrush::getInstance();

        oilbrush.setColor(Qt::white);
        QColor white_color = oilbrush.getColor();

        oilbrush.setColor(Qt::red);
        QColor red_color = oilbrush.getColor();

        oilbrush.setColor(Qt::blue);
        QColor blue_color = oilbrush.getColor();

        oilbrush.setColor(QColor(111, 32, 243));
        QColor rgb_color = oilbrush.getColor();

        oilbrush.setColor(Qt::black);
        QColor black_color = oilbrush.getColor();

        REQUIRE(white_color == Qt::white);
        REQUIRE(red_color == Qt::red);
        REQUIRE(blue_color == Qt::blue);
        REQUIRE(rgb_color == QColor(111, 32, 243));
        REQUIRE(black_color == Qt::black);
    }

    SECTION("Default base_pen style, cap and join") {
        MyTestOilBrush test_oilbrush;

        int style = test_oilbrush.getBasePenStyle();
        int cap = test_oilbrush.getBasePenCap();
        int join = test_oilbrush.getBasePenJoin();

        REQUIRE(style == Qt::SolidLine);
        REQUIRE(cap == Qt::SquareCap);
        REQUIRE(join == Qt::BevelJoin);
    }

    SECTION("Variable new_distance is adjusted based on point provided in the method draw") {
        OilBrush& oilbrush = OilBrush::getInstance();
        MyTestOilBrush test_oilbrush;

        QPoint set_last_position(5, 5);
        test_oilbrush.setLastPosition(set_last_position);

        QPixmap pixmap(1000, 1000);
        QPoint point(10, 10);
        oilbrush.startDrawing(pixmap, point);

        int old_distance = oilbrush.getDistance();
        oilbrush.draw(point);
        int new_distance = test_oilbrush.getNewDistance();
        oilbrush.stoppedDrawing();

        QPoint new_point = point - set_last_position;
        int new_point_distance = new_point.manhattanLength();
        int expected_new_distance = old_distance - new_point_distance;

        REQUIRE(new_distance == expected_new_distance);
    }
}
