#ifndef MYTESTWATERBRUSH_H
#define MYTESTWATERBRUSH_H

#include "../../src/WaterBrush/waterbrush.h"


class MyTestWaterBrush
{
public:
    MyTestWaterBrush();

    int getNewDistance();
    int getBasePenStyle();
    int getBasePenCap();
    int getBasePenJoin();
    QPoint getLastPosition();
    void setLastPosition(const QPoint& new_last_position);
};

#endif // MYTESTWATERBRUSH_H
