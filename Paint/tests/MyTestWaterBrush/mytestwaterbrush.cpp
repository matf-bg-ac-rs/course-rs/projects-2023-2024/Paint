#include "mytestwaterbrush.h"


MyTestWaterBrush::MyTestWaterBrush() {}

int MyTestWaterBrush::getNewDistance() {
    WaterBrush& waterbrush = WaterBrush::getInstance();

    return waterbrush.new_distance;
}

int MyTestWaterBrush::getBasePenStyle() {
    WaterBrush& waterbrush = WaterBrush::getInstance();

    return waterbrush.base_pen.style();
}

int MyTestWaterBrush::getBasePenCap() {
    WaterBrush& waterbrush = WaterBrush::getInstance();

    return waterbrush.base_pen.capStyle();
}

int MyTestWaterBrush::getBasePenJoin() {
    WaterBrush& waterbrush = WaterBrush::getInstance();

    return waterbrush.base_pen.joinStyle();
}

QPoint MyTestWaterBrush::getLastPosition() {
    WaterBrush& waterbrush = WaterBrush::getInstance();

    return waterbrush.last_position;
}

void MyTestWaterBrush::setLastPosition(const QPoint& new_last_position) {
    WaterBrush& waterbrush = WaterBrush::getInstance();

    waterbrush.last_position = new_last_position;
}
