#include "catch.hpp"
#include "../src/ProgramState/programstate.h"
#include <cstdio>


TEST_CASE("ProgramState save_state and load_state", "[ProgramState]") {

    SECTION("Saving and loading active colour") {

        // arrange
        QFile file("stanje_programa.json");
        file.remove();
        AppStateInfo originalAppStateInfo;
        AppStateInfo loadedAppStateInfo;
        originalAppStateInfo.setActiveColor(QColor(1, 2, 3));

        // act
        ProgramState::saveState(originalAppStateInfo);
        ProgramState::loadState(loadedAppStateInfo);

        //assert
        REQUIRE(loadedAppStateInfo.activeColor() == originalAppStateInfo.activeColor());
    }

    SECTION("Saving and loading active tool") {

        // arrange
        QFile file("stanje_programa.json");
        file.remove();
        AppStateInfo originalAppStateInfo;
        AppStateInfo loadedAppStateInfo;
        ButtonInfo active_tool = ButtonInfo::ERASER;
        originalAppStateInfo.setActiveTool(active_tool);

        // act
        ProgramState::saveState(originalAppStateInfo);
        ProgramState::loadState(loadedAppStateInfo);

        //assert
        REQUIRE(loadedAppStateInfo.activeColor() == originalAppStateInfo.activeColor());
    }

    SECTION("Saving and loading open files") {

        // arrange
        QFile file("stanje_programa.json");
        file.remove();
        AppStateInfo originalAppStateInfo;
        AppStateInfo loadedAppStateInfo;
        originalAppStateInfo.openFiles() << "file1.txt" << "file2.txt";

        // act
        ProgramState::saveState(originalAppStateInfo);
        ProgramState::loadState(loadedAppStateInfo);

        //assert
        REQUIRE(loadedAppStateInfo.openFiles() == originalAppStateInfo.openFiles());
    }

    SECTION("Checking default values for instances") {

        // arrange
        ButtonInfo expected_tool = ButtonInfo::BRUSH;
        QColor expected_colour = Qt::black;
        AppStateInfo instance;

        // act
        ButtonInfo tool = instance.activeTool();
        QColor colour = instance.activeColor();
        bool is_empty = instance.openFiles().isEmpty();

        // assert
        REQUIRE(tool == expected_tool);
        REQUIRE(colour == expected_colour);
        REQUIRE(is_empty == true);
    }

    SECTION("When load file not exists") {
        // arrange
        AppStateInfo originalAppStateInfo;
        QFile file("stanje_programa.json");

        // act
        file.remove();

        // assert
        REQUIRE_NOTHROW(ProgramState::loadState(originalAppStateInfo));
    }
}
