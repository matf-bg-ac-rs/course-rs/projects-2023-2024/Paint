#include "catch.hpp"
#include "../src/Scene/scene.h"
#include "../src/Panel/panel.h"
#include <QWidget>

TEST_CASE("Inicijalizacija panela i testiranje odgovarajucih metoda za feature multiple panels") {


    SECTION("Adding panels within limit") {

        Scene* scene = new Scene();
        scene->add_panel();
        scene->add_panel();
        scene->add_panel();

        int broj_p = scene->panel_n();
        int index = scene->currentPanel_->getId();

        REQUIRE(broj_p == 4);
        REQUIRE(scene->currentPanel_ != nullptr);
        delete scene;
    }

    SECTION("Promena panela") {
        Panel::resetCounter();
        Scene* scene = new Scene();
        scene->add_panel();
        scene->add_panel();
        scene->add_panel();
        scene->change_panel(1);

        auto panel = scene->currentPanel_;
        auto id = panel->getId();


        REQUIRE(panel != nullptr);
        REQUIRE(id == 2);

        delete scene;
    }

    SECTION("Pokusaj dodavanja vise od maximalnog") {
        Scene* scene = new Scene();
            for (int i = 0; i < MAX_PANELS; ++i) {
            scene->add_panel();
            }
            scene->add_panel();

            auto broj_panela = scene->panel_n();


            REQUIRE(broj_panela == MAX_PANELS);

            delete scene;
        }

    SECTION("Brisanje panela") {
        Panel::resetCounter();
        Scene *scene = new Scene();

        scene->add_panel();
        scene->add_panel();
        scene->delete_panel();
        auto broj_panela = scene->panel_n();
        auto panel = scene->currentPanel_;
        auto id = panel->getId();

        REQUIRE(broj_panela == 2);
        REQUIRE(panel != nullptr);
        REQUIRE(id == 2);

        delete scene;
    }

    SECTION("Brisanje jedinog preostalog panela") {
        Panel::resetCounter();
        Scene *scene = new Scene();


        scene->delete_panel();
        auto broj_panela = scene->panel_n();
        auto panel = scene->currentPanel_;
        auto id = panel->getId();

        REQUIRE(broj_panela == 1);
        REQUIRE(panel != nullptr);
        REQUIRE(id == 1);

        delete scene;
    }
}
