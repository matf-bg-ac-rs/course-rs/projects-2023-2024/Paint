#include "catch.hpp"
#include "../src/Scene/scene.h"
#include <qpainter.h>
#include <qpixmap.h>


TEST_CASE("Testing changing themes functionality", "[Themes]"){
    SECTION("Testing if Clear all functionality works well"){
        // arrange
        QPixmap* pixmap = new QPixmap(1000,1000);
        pixmap->fill(qRgb(255,255,255));

        QPainter* painter = new QPainter(pixmap);
        painter->drawLine(10,10,10,20);

        Scene* scene = new Scene();
        *scene->currentPanel_->pixmap = *pixmap;

        QRgb expected_color = qRgb(255,255,255);

        // act

        scene->clear_all();

        QRgb result_color1 = scene->currentPanel_->pixmap->toImage().pixelColor(10,10).rgb();
        QRgb result_color2 = scene->currentPanel_->pixmap->toImage().pixelColor(10,20).rgb();
        QRgb result_color3 = scene->currentPanel_->pixmap->toImage().pixelColor(10,15).rgb();

        // assert
        REQUIRE(result_color1 == expected_color);
        REQUIRE(result_color2 == expected_color);
        REQUIRE(result_color3 == expected_color);
    }

    SECTION("Testing if Dark theme functionality works well"){
        // arrange
        QPixmap* pixmap = new QPixmap(1000,1000);
        pixmap->fill(qRgb(255,255,255));


        Scene* scene = new Scene();
        *scene->currentPanel_->pixmap = *pixmap;

        QRgb expected_color = qRgb(37,37,38);

        // act

        scene->changeTheme(Panel::Theme::DARK);

        QRgb result_color1 = scene->currentPanel_->pixmap->toImage().pixelColor(10,10).rgb();
        QRgb result_color2 = scene->currentPanel_->pixmap->toImage().pixelColor(150,278).rgb();
        QRgb result_color3 = scene->currentPanel_->pixmap->toImage().pixelColor(348,986).rgb();

        // assert
        REQUIRE(result_color1 == expected_color);
        REQUIRE(result_color2 == expected_color);
        REQUIRE(result_color3 == expected_color);
    }


    SECTION("Testing if Space grey theme functionality works well"){
        // arrange
        QPixmap* pixmap = new QPixmap(1000,1000);
        pixmap->fill(qRgb(255,255,255));


        Scene* scene = new Scene();
        *scene->currentPanel_->pixmap = *pixmap;

        QRgb expected_color = qRgb(79,91,102);

        // act

        scene->changeTheme(Panel::Theme::SPACE);

        QRgb result_color1 = scene->currentPanel_->pixmap->toImage().pixelColor(10,10).rgb();
        QRgb result_color2 = scene->currentPanel_->pixmap->toImage().pixelColor(150,278).rgb();
        QRgb result_color3 = scene->currentPanel_->pixmap->toImage().pixelColor(348,986).rgb();

        // assert
        REQUIRE(result_color1 == expected_color);
        REQUIRE(result_color2 == expected_color);
        REQUIRE(result_color3 == expected_color);
    }


    SECTION("Testing if Lavender theme functionality works well"){
        // arrange
        QPixmap* pixmap = new QPixmap(1000,1000);
        pixmap->fill(qRgb(255,255,255));


        Scene* scene = new Scene();
        *scene->currentPanel_->pixmap = *pixmap;

        QRgb expected_color = qRgb(191,148,228);

        // act

        scene->changeTheme(Panel::Theme::LAVENDER);

        QRgb result_color1 = scene->currentPanel_->pixmap->toImage().pixelColor(10,10).rgb();
        QRgb result_color2 = scene->currentPanel_->pixmap->toImage().pixelColor(150,278).rgb();
        QRgb result_color3 = scene->currentPanel_->pixmap->toImage().pixelColor(348,986).rgb();

        // assert
        REQUIRE(result_color1 == expected_color);
        REQUIRE(result_color2 == expected_color);
        REQUIRE(result_color3 == expected_color);
    }


    SECTION("Testing if Light theme functionality works well with Dark theme previously"){
        // arrange
        QPixmap* pixmap = new QPixmap(1000,1000);
        pixmap->fill(qRgb(37,37,38));


        Scene* scene = new Scene();
        *scene->currentPanel_->pixmap = *pixmap;
        scene->currentPanel_->theme = Panel::Theme::DARK;

        QRgb expected_color = qRgb(255,255,255);

        // act

        scene->changeTheme(Panel::Theme::LIGHT);

        QRgb result_color1 = scene->currentPanel_->pixmap->toImage().pixelColor(10,10).rgb();
        QRgb result_color2 = scene->currentPanel_->pixmap->toImage().pixelColor(879,564).rgb();
        QRgb result_color3 = scene->currentPanel_->pixmap->toImage().pixelColor(348,986).rgb();

        // assert
        REQUIRE(result_color1 == expected_color);
        REQUIRE(result_color2 == expected_color);
        REQUIRE(result_color3 == expected_color);
    }

    SECTION("Testing if Light theme functionality works well with Space grey theme previously"){
        // arrange
        QPixmap* pixmap = new QPixmap(1000,1000);
        pixmap->fill(qRgb(79,91,102));


        Scene* scene = new Scene();
        *scene->currentPanel_->pixmap = *pixmap;
        scene->currentPanel_->theme = Panel::Theme::SPACE;

        QRgb expected_color = qRgb(255,255,255);

        // act

        scene->changeTheme(Panel::Theme::LIGHT);

        QRgb result_color1 = scene->currentPanel_->pixmap->toImage().pixelColor(10,10).rgb();
        QRgb result_color2 = scene->currentPanel_->pixmap->toImage().pixelColor(879,564).rgb();
        QRgb result_color3 = scene->currentPanel_->pixmap->toImage().pixelColor(348,986).rgb();

        // assert
        REQUIRE(result_color1 == expected_color);
        REQUIRE(result_color2 == expected_color);
        REQUIRE(result_color3 == expected_color);
    }

    SECTION("Testing if Light theme functionality works well with Lavender theme previously"){
        // arrange
        QPixmap* pixmap = new QPixmap(1000,1000);
        pixmap->fill(qRgb(191,148,228));


        Scene* scene = new Scene();
        *scene->currentPanel_->pixmap = *pixmap;
        scene->currentPanel_->theme = Panel::Theme::LAVENDER;

        QRgb expected_color = qRgb(255,255,255);

        // act

        scene->changeTheme(Panel::Theme::LIGHT);

        QRgb result_color1 = scene->currentPanel_->pixmap->toImage().pixelColor(10,10).rgb();
        QRgb result_color2 = scene->currentPanel_->pixmap->toImage().pixelColor(879,564).rgb();
        QRgb result_color3 = scene->currentPanel_->pixmap->toImage().pixelColor(348,986).rgb();

        // assert
        REQUIRE(result_color1 == expected_color);
        REQUIRE(result_color2 == expected_color);
        REQUIRE(result_color3 == expected_color);
    }
}








