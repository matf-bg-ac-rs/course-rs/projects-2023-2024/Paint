#include "catch.hpp"
#include "../src/Shape/shape.h"
#include "../src/Rectangle/rectangle.h"
#include "../src/Square/square.h"
#include "../src/Triangle/triangle.h"
#include "../src/Circle/circle.h"
#include "../src/Line/line.h"


TEST_CASE("Testing Rectangle class", "[Rectangle]"){

    SECTION("Testing if getInstance method returns valid instance"){
        // Arrange

        // Act
        Rectangle& rectangle = Rectangle::getInstance();

        // Assert
        REQUIRE_FALSE(&rectangle == nullptr);
    }

    SECTION("Testing if getInstance method returns same instances every time") {
        // Arrange

        // Act
        Rectangle& rectangle1 = Rectangle::getInstance();
        Rectangle& rectangle2 = Rectangle::getInstance();

        // Assert
        REQUIRE(&rectangle1 == &rectangle2);
    }

    SECTION("Testing if Type method works properly"){
        // Arrange
        Rectangle& rectangle = Rectangle::getInstance();
        QString expected_type = "Rectangle";

        // Act
        QString result_type = rectangle.Type();

        // Assert
        REQUIRE(expected_type == result_type);
    }

    SECTION("Testing if setX method works properly"){
        // Arrange
        Rectangle& rectangle = Rectangle::getInstance();
        int expected_x = 18;

        // Act
        rectangle.setX(18);
        int result_x = rectangle.getX();

        // Assert
        REQUIRE(expected_x == result_x);
    }

    SECTION("Testing if setY method works properly"){
        // Arrange
        Rectangle& rectangle = Rectangle::getInstance();
        int expected_y = 18;

        // Act
        rectangle.setY(18);
        int result_y = rectangle.getY();

        // Assert
        REQUIRE(expected_y == result_y);
    }

    SECTION("Testing if setSize method works properly"){
        // Arrange
        Rectangle& rectangle = Rectangle::getInstance();
        int expected_size = 18;

        // Act
        rectangle.setSize(18);
        int result_size = rectangle.getSize();

        // Assert
        REQUIRE(expected_size == result_size);

        // Reset (because Rectangle is a singleton class)
        rectangle.setSize(8);
    }

    SECTION("Testing if setColor method works properly"){
        // Arrange
        Rectangle& rectangle = Rectangle::getInstance();
        QColor expected_color = QColor(0, 0, 1); // blue

        // Act
        rectangle.setColor(QColor(0, 0, 1)); // blue
        QColor result_color = rectangle.getColor();

        // Assert
        REQUIRE(expected_color == result_color);

        // Reset (because Rectangle is a singleton class)
        rectangle.setColor(QColor(Qt::black));
    }

    SECTION("Testing if the default value of size is 8"){
        // Arrange
        Rectangle& rectangle = Rectangle::getInstance();
        int expected_size = 8;

        // Act
        int result_size = rectangle.getSize();

        // Assert
        REQUIRE(expected_size == result_size);
    }

    SECTION("Testing if the default value of color is black"){
        // Arrange
        Rectangle& rectangle = Rectangle::getInstance();
        QColor expected_color = QColor(Qt::black);

        // Act
        QColor result_color = rectangle.getColor();

        // Assert
        REQUIRE(expected_color.red() == result_color.red());
        REQUIRE(expected_color.green() == result_color.green());
        REQUIRE(expected_color.blue() == result_color.blue());
    }

    SECTION("Testing if the default value of width is 30"){
        // Arrange
        Rectangle& rectangle = Rectangle::getInstance();
        int expected_width = 30;

        // Act
        int result_width = rectangle.getWidth();

        // Assert
        REQUIRE(expected_width == result_width);
    }

    SECTION("Testing if the default value of height is 20"){
        // Arrange
        Rectangle& rectangle = Rectangle::getInstance();
        int expected_height = 20;

        // Act
        int result_height = rectangle.getHeight();

        // Assert
        REQUIRE(expected_height == result_height);
    }

    SECTION("Testing if setWidth method works properly"){
        // Arrange
        Rectangle& rectangle = Rectangle::getInstance();
        int expected_width = 18;

        // Act
        rectangle.setWidth(18);
        int result_width = rectangle.getWidth();

        // Assert
        REQUIRE(expected_width == result_width);
    }

    SECTION("Testing if setHeight method works properly"){
        // Arrange
        Rectangle& rectangle = Rectangle::getInstance();
        int expected_height = 18;

        // Act
        rectangle.setHeight(18);
        int result_height = rectangle.getHeight();

        // Assert
        REQUIRE(expected_height == result_height);
    }
}


TEST_CASE("Testing Square class", "[Square]"){

    SECTION("Testing if getInstance method returns valid instance"){
        // Arrange

        // Act
        Square& Square = Square::getInstance();

        // Assert
        REQUIRE_FALSE(&Square == nullptr);
    }

    SECTION("Testing if getInstance method returns same instances every time") {
        // Arrange

        // Act
        Square& square1 = Square::getInstance();
        Square& square2 = Square::getInstance();

        // Assert
        REQUIRE(&square1 == &square2);
    }

    SECTION("Testing if Type method works properly"){
        // Arrange
        Square& square = Square::getInstance();
        QString expected_type = "Square";

        // Act
        QString result_type = square.Type();

        // Assert
        REQUIRE(expected_type == result_type);
    }

    SECTION("Testing if setX method works properly"){
        // Arrange
        Square& square = Square::getInstance();
        int expected_x = 18;

        // Act
        square.setX(18);
        int result_x = square.getX();

        // Assert
        REQUIRE(expected_x == result_x);
    }

    SECTION("Testing if setY method works properly"){
        // Arrange
        Square& square = Square::getInstance();
        int expected_y = 18;

        // Act
        square.setY(18);
        int result_y = square.getY();

        // Assert
        REQUIRE(expected_y == result_y);
    }

    SECTION("Testing if setSize method works properly"){
        // Arrange
        Square& square = Square::getInstance();
        int expected_size = 18;

        // Act
        square.setSize(18);
        int result_size = square.getSize();

        // Assert
        REQUIRE(expected_size == result_size);

        // Reset (because Square is a singleton class)
        square.setSize(8);
    }

    SECTION("Testing if setColor method works properly"){
        // Arrange
        Square& square = Square::getInstance();
        QColor expected_color = QColor(Qt::blue);

        // Act
        square.setColor(QColor(Qt::blue));
        QColor result_color = square.getColor();

        // Assert
        REQUIRE(expected_color == result_color);

        // Reset (because Square is a singleton class)
        square.setColor(QColor(Qt::black));
    }

    SECTION("Testing if the default value of size is 8"){
        // Arrange
        Square& square = Square::getInstance();
        int expected_size = 8;

        // Act
        int result_size = square.getSize();

        // Assert
        REQUIRE(expected_size == result_size);
    }

    SECTION("Testing if the default value of color is black"){
        // Arrange
        Square& square = Square::getInstance();
        QColor expected_color = QColor(Qt::black);

        // Act
        QColor result_color = square.getColor();

        // Assert
        REQUIRE(expected_color.red() == result_color.red());
        REQUIRE(expected_color.green() == result_color.green());
        REQUIRE(expected_color.blue() == result_color.blue());
    }

    SECTION("Testing if the default value of width is 20"){
        // Arrange
        Square& square = Square::getInstance();
        int expected_width = 20;

        // Act
        int result_width = square.getWidth();

        // Assert
        REQUIRE(expected_width == result_width);
    }

    SECTION("Testing if the default value of height is 20"){
        // Arrange
        Square& square = Square::getInstance();
        int expected_height = 20;

        // Act
        int result_height = square.getHeight();

        // Assert
        REQUIRE(expected_height == result_height);
    }

    SECTION("Testing if the default value of side is 20"){
        // Arrange
        Square& square = Square::getInstance();
        int expected_side = 20;

        // Act
        int result_side = square.getSide();

        // Assert
        REQUIRE(expected_side == result_side);
    }

    SECTION("Testing if setWidth method works properly"){
        // Arrange
        Square& square = Square::getInstance();
        int expected_width = 18;

        // Act
        square.setWidth(18);
        int result_width = square.getWidth();

        // Assert
        REQUIRE(expected_width == result_width);
    }

    SECTION("Testing if setHeight method works properly"){
        // Arrange
        Square& square = Square::getInstance();
        int expected_height = 18;

        // Act
        square.setHeight(18);
        int result_height = square.getHeight();

        // Assert
        REQUIRE(expected_height == result_height);
    }

    SECTION("Testing if setHeight method works properly"){
        // Arrange
        Square& square = Square::getInstance();
        int expected_side = 18;

        // Act
        square.setSide(18);
        int result_side = square.getSide();

        // Assert
        REQUIRE(expected_side == result_side);
    }
}


TEST_CASE("Testing Triangle class", "[Triangle]"){

SECTION("Testing if getInstance method returns valid instance"){
        // Arrange

        // Act
        Triangle& triangle = Triangle::getInstance();

        // Assert
        REQUIRE_FALSE(&triangle == nullptr);
    }

    SECTION("Testing if getInstance method returns same instances every time") {
        // Arrange

        // Act
        Triangle& triangle1 = Triangle::getInstance();
        Triangle& triangle2 = Triangle::getInstance();

        // Assert
        REQUIRE(&triangle1 == &triangle2);
    }

    SECTION("Testing if Type method works properly"){
        // Arrange
        Triangle& triangle = Triangle::getInstance();
        QString expected_type = "Triangle";

        // Act
        QString result_type = triangle.Type();

        // Assert
        REQUIRE(expected_type == result_type);
    }

    SECTION("Testing if setX method works properly"){
        // Arrange
        Triangle& triangle = Triangle::getInstance();
        int expected_x = 18;

        // Act
        triangle.setX(18);
        int result_x = triangle.getX();

        // Assert
        REQUIRE(expected_x == result_x);
    }

    SECTION("Testing if setY method works properly"){
        // Arrange
        Triangle& triangle = Triangle::getInstance();
        int expected_y = 18;

        // Act
        triangle.setY(18);
        int result_y = triangle.getY();

        // Assert
        REQUIRE(expected_y == result_y);
    }

    SECTION("Testing if setSize method works properly"){
        // Arrange
        Triangle& triangle = Triangle::getInstance();
        int expected_size = 18;

        // Act
        triangle.setSize(18);
        int result_size = triangle.getSize();

        // Assert
        REQUIRE(expected_size == result_size);

        // Reset (because Triangle is a singleton class)
        triangle.setSize(8);
    }

    SECTION("Testing if setColor method works properly"){
        // Arrange
        Triangle& triangle = Triangle::getInstance();
        QColor expected_color = QColor(Qt::blue);

        // Act
        triangle.setColor(QColor(Qt::blue));
        QColor result_color = triangle.getColor();

        // Assert
        REQUIRE(expected_color == result_color);

        // Reset (because Triangle is a singleton class)
        triangle.setColor(QColor(Qt::black));
    }

    SECTION("Testing if the default value of size is 8"){
        // Arrange
        Triangle& triangle = Triangle::getInstance();
        int expected_size = 8;

        // Act
        int result_size = triangle.getSize();

        // Assert
        REQUIRE(expected_size == result_size);
    }

    SECTION("Testing if the default value of color is black"){
        // Arrange
        Triangle& triangle = Triangle::getInstance();
        QColor expected_color = QColor(Qt::black);

        // Act
        QColor result_color = triangle.getColor();

        // Assert
        REQUIRE(expected_color.red() == result_color.red());
        REQUIRE(expected_color.green() == result_color.green());
        REQUIRE(expected_color.blue() == result_color.blue());
    }

    SECTION("Testing if the default value of base is 20"){
        // Arrange
        Triangle& triangle = Triangle::getInstance();
        int expected_base = 20;

        // Act
        int result_base = triangle.getBase();

        // Assert
        REQUIRE(expected_base == result_base);
    }

    SECTION("Testing if the default value of height is 20"){
        // Arrange
        Triangle& triangle = Triangle::getInstance();
        int expected_height = 20;

        // Act
        int result_height = triangle.getHeight();

        // Assert
        REQUIRE(expected_height == result_height);
    }

    SECTION("Testing if the setBase method is working properly"){
        // Arrange
        Triangle& triangle = Triangle::getInstance();
        int expected_base = 18;

        // Act
        triangle.setBase(18);
        int result_base = triangle.getBase();

        // Assert
        REQUIRE(expected_base == result_base);
    }

    SECTION("Testing if the setHeight method is working properly"){
        // Arrange
        Triangle& triangle = Triangle::getInstance();
        int expected_height = 18;

        // Act
        triangle.setHeight(18);
        int result_height = triangle.getHeight();

        // Assert
        REQUIRE(expected_height == result_height);
    }
}


TEST_CASE("Testing Circle class", "[Circle]"){

SECTION("Testing if getInstance method returns valid instance"){
        // Arrange

        // Act
        Circle& circle = Circle::getInstance();

        // Assert
        REQUIRE_FALSE(&circle == nullptr);
    }

    SECTION("Testing if getInstance method returns same instances every time") {
        // Arrange

        // Act
        Circle& circle1 = Circle::getInstance();
        Circle& circle2 = Circle::getInstance();

        // Assert
        REQUIRE(&circle1 == &circle2);
    }

    SECTION("Testing if Type method works properly"){
        // Arrange
        Circle& circle = Circle::getInstance();
        QString expected_type = "Circle";

        // Act
        QString result_type = circle.Type();

        // Assert
        REQUIRE(expected_type == result_type);
    }

    SECTION("Testing if setX method works properly"){
        // Arrange
        Circle& circle = Circle::getInstance();
        int expected_x = 18;

        // Act
        circle.setX(18);
        int result_x = circle.getX();

        // Assert
        REQUIRE(expected_x == result_x);
    }

    SECTION("Testing if setY method works properly"){
        // Arrange
        Circle& circle = Circle::getInstance();
        int expected_y = 18;

        // Act
        circle.setY(18);
        int result_y = circle.getY();

        // Assert
        REQUIRE(expected_y == result_y);
    }

    SECTION("Testing if setSize method works properly"){
        // Arrange
        Circle& circle = Circle::getInstance();
        int expected_size = 18;

        // Act
        circle.setSize(18);
        int result_size = circle.getSize();

        // Assert
        REQUIRE(expected_size == result_size);

        // Reset (because Circle is a singleton class)
        circle.setSize(8);
    }

    SECTION("Testing if setColor method works properly"){
        // Arrange
        Circle& circle = Circle::getInstance();
        QColor expected_color = QColor(Qt::blue);

        // Act
        circle.setColor(QColor(Qt::blue));
        QColor result_color = circle.getColor();

        // Assert
        REQUIRE(expected_color == result_color);

        // Reset (because Circle is a singleton class)
        circle.setColor(QColor(Qt::black));
    }

    SECTION("Testing if the default value of size is 8"){
        // Arrange
        Circle& circle = Circle::getInstance();
        int expected_size = 8;

        // Act
        int result_size = circle.getSize();

        // Assert
        REQUIRE(expected_size == result_size);
    }

    SECTION("Testing if the default value of color is black"){
        // Arrange
        Circle& circle = Circle::getInstance();
        QColor expected_color = QColor(Qt::black);

        // Act
        QColor result_color = circle.getColor();

        // Assert
        REQUIRE(expected_color.red() == result_color.red());
        REQUIRE(expected_color.green() == result_color.green());
        REQUIRE(expected_color.blue() == result_color.blue());
    }

    SECTION("Testing if the default value of radius is 20"){
        // Arrange
        Circle& circle = Circle::getInstance();
        int expected_radius = 20;

        // Act
        int result_radius = circle.getRadius();

        // Assert
        REQUIRE(expected_radius == result_radius);
    }

    SECTION("Testing if setRadius method works properly"){
        // Arrange
        Circle& circle = Circle::getInstance();
        int expected_radius = 18;

        // Act
        circle.setRadius(18);
        int result_radius = circle.getRadius();

        // Assert
        REQUIRE(expected_radius == result_radius);
    }
}


TEST_CASE("Testing Line class", "[Line]"){

    SECTION("Testing if getInstance method returns valid instance"){
        // Arrange

        // Act
        Line& line = Line::getInstance();

        // Assert
        REQUIRE_FALSE(&line == nullptr);
    }

    SECTION("Testing if getInstance method returns same instances every time") {
        // Arrange

        // Act
        Line& line1 = Line::getInstance();
        Line& line2 = Line::getInstance();

        // Assert
        REQUIRE(&line1 == &line2);
    }

    SECTION("Testing if Type method works properly"){
        // Arrange
        Line& line = Line::getInstance();
        QString expected_type = "Line";

        // Act
        QString result_type = line.Type();

        // Assert
        REQUIRE(expected_type == result_type);
    }

    SECTION("Testing if setX method works properly"){
        // Arrange
        Line& line = Line::getInstance();
        int expected_x = 18;

        // Act
        line.setX(18);
        int result_x = line.getX();

        // Assert
        REQUIRE(expected_x == result_x);
    }

    SECTION("Testing if setY method works properly"){
        // Arrange
        Line& line = Line::getInstance();
        int expected_y = 18;

        // Act
        line.setY(18);
        int result_y = line.getY();

        // Assert
        REQUIRE(expected_y == result_y);
    }

    SECTION("Testing if setSize method works properly"){
        // Arrange
        Line& line = Line::getInstance();
        int expected_size = 18;

        // Act
        line.setSize(18);
        int result_size = line.getSize();

        // Assert
        REQUIRE(expected_size == result_size);

        // Reset (because Line is a singleton class)
        line.setSize(8);
    }

    SECTION("Testing if setColor method works properly"){
        // Arrange
        Line& line = Line::getInstance();
        QColor expected_color = QColor(Qt::blue);

        // Act
        line.setColor(QColor(Qt::blue));
        QColor result_color = line.getColor();

        // Assert
        REQUIRE(expected_color == result_color);

        // Reset (because Line is a singleton class)
        line.setColor(QColor(Qt::black));
    }

    SECTION("Testing if the default value of size is 8"){
        // Arrange
        Line& line = Line::getInstance();
        int expected_size = 8;

        // Act
        int result_size = line.getSize();

        // Assert
        REQUIRE(expected_size == result_size);
    }

    SECTION("Testing if the default value of color is black"){
        // Arrange
        Line& line = Line::getInstance();
        QColor expected_color = QColor(Qt::black);

        // Act
        QColor result_color = line.getColor();

        // Assert
        REQUIRE(expected_color.red() == result_color.red());
        REQUIRE(expected_color.green() == result_color.green());
        REQUIRE(expected_color.blue() == result_color.blue());
    }

    SECTION("Testing if the default value of start is (0, 0)"){
        // Arrange
        Line& line = Line::getInstance();
        QPoint expected_start = QPoint(0, 0);

        // Act
        QPoint result_start = line.getStart();

        // Assert
        REQUIRE(expected_start == result_start);
    }

    SECTION("Testing if the default value of end is (0, 0)"){
        // Arrange
        Line& line = Line::getInstance();
        QPoint expected_end = QPoint(0,0);

        // Act
        QPoint result_end = line.getEnd();

        // Assert
        REQUIRE(expected_end == result_end);
    }

    SECTION("Testing if setStart method works properly"){
        // Arrange
        Line& line = Line::getInstance();
        QPoint expected_start = QPoint(18, 18);

        // Act
        line.setStart(QPoint(18,18));
        QPoint result_start = line.getStart();

        // Assert
        REQUIRE(expected_start == result_start);

        // Reset (because Line is a singleton class)
        line.setStart(QPoint(0, 0));
    }

    SECTION("Testing if setEnd method works properly"){
        // Arrange
        Line& line = Line::getInstance();
        QPoint expected_end = QPoint(18, 18);

        // Act
        line.setEnd(QPoint(18,18));
        QPoint result_end = line.getEnd();

        // Assert
        REQUIRE(expected_end == result_end);

        // Reset (because Line is a singleton class)
        line.setEnd(QPoint(0, 0));
    }
}
