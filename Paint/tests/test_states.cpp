#include "catch.hpp"
#include "../src/States/states.cpp"

#include<QVector>

TEST_CASE("Testovi za strukturu podataka stanja(states)", "[States]") {

    SECTION("Inicijalizacije") {
        States<int> intStates;
        auto undo = intStates.undo();
        auto redo = intStates.redo();
        auto active = intStates.getActive();

        REQUIRE(undo == nullptr);
        REQUIRE(redo == nullptr);
        REQUIRE(active == nullptr);
    }

    SECTION("Undo and redo operacije") {
        States<int> intStates;
        intStates.onChange(new int(1));
        intStates.onChange(new int(2));

        int* undoResult = intStates.undo();
        int* redoResult = intStates.redo();

        REQUIRE(undoResult != nullptr);
        REQUIRE(*undoResult == 1);

        REQUIRE(redoResult != nullptr);
        REQUIRE(*redoResult == 2);

        delete undoResult;
        delete redoResult;
    }




    SECTION("Undo van granica") {
        States<int> intStates;
        intStates.onChange(new int(1));
        intStates.onChange(new int(2));

        int* undoResult = intStates.undo();
        int* secondUndoResult = intStates.undo();


        REQUIRE(undoResult != nullptr);
        REQUIRE(*undoResult == 1);
        REQUIRE(secondUndoResult == nullptr);


        delete undoResult;
        delete secondUndoResult;
    }

    SECTION("Redo van granica") {
        States<int> intStates;
        intStates.onChange(new int(1));

        int* redoResult = intStates.redo();
        REQUIRE(redoResult == nullptr);

        delete redoResult;
    }

    SECTION("Uzimanje aktivnih stanja") {

        States<int> intStates;
        intStates.onChange(new int(1));
        int* activeState = intStates.getActive();


        REQUIRE(activeState != nullptr);
        REQUIRE(*activeState == 1);

        delete activeState;
    }
}
