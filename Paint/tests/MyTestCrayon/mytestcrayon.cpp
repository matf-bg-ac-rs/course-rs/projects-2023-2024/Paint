#include "mytestcrayon.h"


MyTestCrayon::MyTestCrayon() {}

int MyTestCrayon::getRadiusPenSize() {
    Crayon& crayon = Crayon::getInstance();

    return crayon.radius_pen_.width();
}

QColor MyTestCrayon::getBaseLighterColor() {
    Crayon& crayon = Crayon::getInstance();

    return crayon.base_lighter_color_.rgb();
}

QColor MyTestCrayon::getRadiusLighterColor() {
    Crayon& crayon = Crayon::getInstance();

    return crayon.radius_lighter_color_.rgb();
}

int MyTestCrayon::getBaseAlpha() {
    Crayon& crayon = Crayon::getInstance();

    return crayon.base_lighter_color_.alpha();
}

int MyTestCrayon::getRadiusAlpha() {
    Crayon& crayon = Crayon::getInstance();

    return crayon.radius_lighter_color_.alpha();
}
