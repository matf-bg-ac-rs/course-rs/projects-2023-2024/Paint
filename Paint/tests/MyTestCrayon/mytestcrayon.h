#ifndef MYTESTCRAYON_H
#define MYTESTCRAYON_H

#include "../../src/Crayon/crayon.h"


class MyTestCrayon
{
public:
    MyTestCrayon();

    int getRadiusPenSize();
    QColor getBaseLighterColor();
    QColor getRadiusLighterColor();
    int getBaseAlpha();
    int getRadiusAlpha();
};

#endif // MYTESTCRAYON_H
