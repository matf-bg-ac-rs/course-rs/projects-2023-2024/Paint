#include "catch.hpp"
#include "../src/ColorButton/colorbutton.h"
#include "../src/ActiveButton/activebutton.h"


TEST_CASE("Provera dugmica ActiveButton i ColorButton", "[Buttons]") {
    SECTION("ColorButton konstruktor") {
        ColorButton* button = new ColorButton(nullptr);

        auto stanje = ColorButton::colorButtons.empty();
        auto dugme = ColorButton::colorButtons.back();

        REQUIRE_FALSE(stanje);
        REQUIRE(dugme == button);
        delete button;
    }

    /*SECTION("ColorButton Connection to color_init Slot") {
        ColorButton* button = new ColorButton(nullptr);
        // Simulate a button click
        button->click();
        // Verify that color_ready signal was emitted
        REQUIRE(button->color_ready_emitted);
        delete button; // Clean up the memory
    }*/
    SECTION("Vracanje ispravne boje") {
        ColorButton* button = new ColorButton(nullptr);

        button->setButtonColor(QColor("red"));
        QColor rez = button->returnColor();
        QColor boja = QColor("red");

        CHECK(rez.blue()==boja.blue());
        CHECK(rez.red() == boja.red());
        CHECK(rez.green() == boja.green());
        delete button;
    }

    SECTION("Kreiranje dugmica i proveravanje odgovarajuceg niza") {
        ActiveButton* button = new ActiveButton(nullptr);


        auto stanje = ActiveButton::actButtons.empty();
        auto dugme = ActiveButton::actButtons.back();

        REQUIRE_FALSE(stanje);
        REQUIRE(dugme == button);
        delete button;
    }

    SECTION("make_code Sets ButtonInfo Property") {
        ActiveButton* button = new ActiveButton(nullptr);
        button->setProperty("code", "Pencil");

        button->make_code();
        auto kod = button->getInfo();


        REQUIRE(kod == ButtonInfo::PENCIL);
        delete button;
    }
    SECTION("Provera da li aktiveButton moze da se lepo poveze na klik") {
        ActiveButton* button = new ActiveButton(nullptr);
        bool signalEmitted = false;
        QObject::connect(button, &ActiveButton::activated, [&signalEmitted](ButtonInfo info) {
            signalEmitted = true;
        });

        button->click();

        REQUIRE(signalEmitted);
        delete button;
    }
}


