#include "catch.hpp"

#include "../src/Tool/tool.h"
#include "../src/ColorPicker/colorpicker.h"

#include <qpixmap.h>

TEST_CASE("Testing ColorPicker class", "[ColorPicker]") {

    SECTION("Testing if getInstance method returns valid instance"){
        // arrange


        // act
        ColorPicker& picker = ColorPicker::getInstance();

        // assert
        REQUIRE_FALSE(&picker == nullptr);
    }

    SECTION("Testing if getInstance method returns same instances every time") {
        // arrange


        // act
        ColorPicker& picker1 = ColorPicker::getInstance();
        ColorPicker& picker2 = ColorPicker::getInstance();

        // assert
        REQUIRE(&picker1 == &picker2);
    }

    SECTION("Testing if colorpicker finds right color for specific pixel"){
        // arrange
        QPixmap* pixmap = new QPixmap(1000,1000);
        pixmap->fill(qRgb(100,100,100));
        ColorPicker& picker = ColorPicker::getInstance();
        QRgb expected_color = qRgb(100,100,100);
        QPoint position(5,5);

        // act
        picker.performAction(position,pixmap);
        QRgb result_color = picker.getColor();

        // assert
        REQUIRE(result_color == expected_color);
        delete pixmap;
    }

    SECTION("Testing if colorpicker sets color properly"){
        // arrange
        ColorPicker& picker = ColorPicker::getInstance();
        QRgb expected_color = qRgb(150,150,150);

        // act
        picker.setColor(qRgb(150,150,150));
        QRgb result_color = picker.getColor();

        // assert
        REQUIRE(result_color == expected_color);
    }

    SECTION("Testing if performAction method doesn't throw exception for valid arguments"){
        // arrange
        QPixmap* pixmap = new QPixmap(1000,1000);
        pixmap->fill(qRgb(100,100,100));
        ColorPicker& picker = ColorPicker::getInstance();
        QPoint position(5,5);

        // act/assert
        REQUIRE_NOTHROW(picker.performAction(position,pixmap));
        delete pixmap;
    }
}
