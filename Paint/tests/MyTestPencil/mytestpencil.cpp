#include "mytestpencil.h"


MyTestPencil::MyTestPencil() {}

QColor MyTestPencil::getPaintColor() {
    Pencil& pencil = Pencil::getInstance();

    return pencil.paint_color_;
}
