#ifndef MYTESTPENCIL_H
#define MYTESTPENCIL_H

#include "../../src/Pencil/pencil.h"


class MyTestPencil
{
public:
    MyTestPencil();

    QColor getPaintColor();
};

#endif // MYTESTPENCIL_H
