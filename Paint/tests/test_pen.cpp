#include "catch.hpp"

#include "../src/Pen/pen.h"
#include "MyTestPen/mytestpen.h"


TEST_CASE("Test case for class Pen", "[Pen]") {
    SECTION("Class initialization and checking default fields") {
        Pen& pen = Pen::getInstance();

        QColor pen_color = pen.getColor();
        int pen_size = pen.getSize();
        int pen_radius = pen.getRadius();

        REQUIRE(pen_color == Qt::black);
        REQUIRE(pen_size == 8);
        REQUIRE(pen_radius == 0);
    }

    SECTION("Same instance of Pen is returned (singleton class") {
        Pen& pen1 = Pen::getInstance();
        Pen& pen2 = Pen::getInstance();

        REQUIRE(&pen1 == &pen2);
    }

    SECTION("Maximum size setter") {
        Pen& pen = Pen::getInstance();

        pen.setSize(50);
        int pen_size = pen.getSize();

        REQUIRE(pen_size == 50);
    }

    SECTION("Minimum size setter") {
        Pen& pen = Pen::getInstance();

        pen.setSize(8);
        int pen_size = pen.getSize();

        REQUIRE(pen_size == 8);
    }

    SECTION("Color setters") {
        Pen& pen = Pen::getInstance();

        pen.setColor(Qt::white);
        QColor white_color = pen.getColor();

        pen.setColor(Qt::red);
        QColor red_color = pen.getColor();

        pen.setColor(Qt::blue);
        QColor blue_color = pen.getColor();

        pen.setColor(QColor(233, 113, 89));
        QColor rgb_color = pen.getColor();

        pen.setColor(Qt::black);
        QColor black_color = pen.getColor();

        REQUIRE(white_color == Qt::white);
        REQUIRE(red_color == Qt::red);
        REQUIRE(blue_color == Qt::blue);
        REQUIRE(rgb_color == QColor(233, 113, 89));
        REQUIRE(black_color == Qt::black);
    }

    SECTION("Default base_pen style, cap and join") {
        MyTestPen test_pen;

        int style = test_pen.getBasePenStyle();
        int cap = test_pen.getBasePenCap();
        int join = test_pen.getBasePenJoin();

        REQUIRE(style == Qt::SolidLine);
        REQUIRE(cap == Qt::RoundCap);
        REQUIRE(join == Qt::RoundJoin);
    }

    SECTION("Default last_position") {
        MyTestPen test_pen;

        QPoint last_position = test_pen.getLastPosition();

        REQUIRE(last_position == QPoint(0, 0));
    }

    SECTION("Setting last_position to default after stoppedDrawing() was called") {
        MyTestPen test_pen;

        QPoint last_position = test_pen.getPosAfterStoppedDrawing();

        REQUIRE(last_position == QPoint(0, 0));
    }

    SECTION("Setting last_position to point after draw(const QPoint point, QPainter& painter) was called") {
        MyTestPen test_pen;
        QPoint point(10, 10);

        QPoint last_position = test_pen.getPosAfterDraw(point);

        REQUIRE(last_position == point);
    }

    SECTION("Check if point is colored in default black after draw(const QPoint point, QPainter& painter) was called") {
        Pen& pen = Pen::getInstance();

        QPixmap pixmap(1000, 1000);
        QPoint point(10, 10);

        pen.startDrawing(pixmap, point);
        pen.draw(point);
        pen.stoppedDrawing();
        QColor colored_pixel = pixmap.toImage().pixelColor(point);

        REQUIRE(colored_pixel.rgb() == QColor(Qt::black).rgb());
    }

    SECTION("Check if color has lightened for a factor after lighterColor(const QColor old_color, const int factor) was called") {
        MyTestPen test_pen;

        QColor lighter_black = test_pen.getLighterColor(QColor(Qt::black), 80);
        QColor lighter_white = test_pen.getLighterColor(QColor(Qt::white), 70);
        QColor lighter_rgb = test_pen.getLighterColor(QColor(233, 100, 253), 50);

        REQUIRE(lighter_black == QColor(80, 80, 80));
        REQUIRE(lighter_white == QColor(255, 255, 255));
        REQUIRE(lighter_rgb == QColor(255, 150, 255));
    }
}
