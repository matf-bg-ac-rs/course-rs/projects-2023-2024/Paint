#include "catch.hpp"
#include "../src/Main/mainwindow.h"


TEST_CASE("Testing controller", "[MainWindow]") {
    SECTION("Testing slot dimension") {
        // arrange
        QFile file("stanje_programa.json");
        file.remove();
        MainWindow w;
        w.sceneClose();
        int expected_dimension = 10;

        // act
        w.dimension(expected_dimension);

        // assert
        REQUIRE(w.active_dimension_ == expected_dimension);
    }
    SECTION("Testing slot color"){
        // arrange
        QFile file("stanje_programa.json");
        file.remove();
        MainWindow w;
        w.sceneClose();
        QColor q(1,2,3);

        // act
        w.color(q);

        // assert
        REQUIRE(w.active_color_.rgb() == q.rgb());
    }

    SECTION("Testing activator with valid value"){
        // arrange
        QFile file("stanje_programa.json");
        file.remove();
        MainWindow w;
        w.sceneClose();
        MainWindow::Active expected_active_type = MainWindow::Active::PEN;
        Pen* p_expected = &(Pen::getInstance());

        // act
        w.activator(ButtonInfo::BRUSH);

        // assert
        REQUIRE(w.activeInteractor() == expected_active_type);
        REQUIRE(w.activePen() == p_expected);
    }

    SECTION("Testing activator for tool"){
        // arrange
        QFile file("stanje_programa.json");
        file.remove();
        MainWindow w;
        w.sceneClose();
        MainWindow::Active expected_active_type = MainWindow::Active::TOOL;
        Tool* t_expected = &(Bucket::getInstance());
        bool exp_sampler_active = false;

        // act
        w.activator(ButtonInfo::BUCKET);

        // assert
        REQUIRE(w.activeInteractor() == expected_active_type);
        REQUIRE(w.activeTool() == t_expected);
        REQUIRE(w.getSamplerActive() == exp_sampler_active);
    }


    SECTION("Testing activator with invalid value"){
        // arrange
        QFile file("stanje_programa.json");
        file.remove();
        MainWindow w;
        w.sceneClose();
        MainWindow::Active expected_active_type = w.activeInteractor();
        Pen* p_expected = &(Pen::getInstance()); // Default value if "stanje_programa.json" is deleted

        // act
        w.activator((ButtonInfo)20);

        // assert
        REQUIRE(w.activeInteractor() == expected_active_type);
        REQUIRE(w.activePen() == p_expected);
    }
}
