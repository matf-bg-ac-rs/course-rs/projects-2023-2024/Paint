#include "catch.hpp"
#include "../src/ProgramState/programstate.h"

TEST_CASE("AppStateInfo toVariant and fromQVariant", "[AppStateInfo]") {
    SECTION("Conversion with valid data") {
        // arrange
        AppStateInfo originalAppStateInfo;
        originalAppStateInfo.openFiles() << "file1.txt" << "file2.txt";
        originalAppStateInfo.setActiveTool(ButtonInfo::BRUSH);
        originalAppStateInfo.setActiveColor(QColor(255, 0, 0));
        AppStateInfo convertedAppStateInfo;
        QVariant variant;

        // act
        variant = originalAppStateInfo.toVariant();
        convertedAppStateInfo.fromQVariant(variant);

        // assert
        REQUIRE(convertedAppStateInfo.openFiles() == originalAppStateInfo.openFiles());
        REQUIRE(convertedAppStateInfo.activeTool() == originalAppStateInfo.activeTool());
        REQUIRE(convertedAppStateInfo.activeColor() == originalAppStateInfo.activeColor());
    }

    SECTION("Conversion with empty data") {
        // arrange
        AppStateInfo originalAppStateInfo;
        QVariant variant = originalAppStateInfo.toVariant();
        AppStateInfo convertedAppStateInfo;

        // act
        convertedAppStateInfo.fromQVariant(variant);

        // assert
        REQUIRE(convertedAppStateInfo.openFiles().isEmpty());
        REQUIRE(convertedAppStateInfo.activeTool() == ButtonInfo::BRUSH);
        REQUIRE(convertedAppStateInfo.activeColor() == QColor(0, 0, 0));
    }

    SECTION("Conversion with invalid ChosenTool string") {
        // arrange
        QVariantMap map;
        map["OpenedFiles"] = QVariant(QStringList() << "file1.txt" << "file2.txt");
        map["ChosenTool"] = QVariant("InvalidTool");
        map["ActiveColour"] = QVariant(QVariantMap{
            {"Red", 255},
            {"Green", 0},
            {"Blue", 0}
        });
        AppStateInfo convertedAppStateInfo;

        // act
        convertedAppStateInfo.fromQVariant(QVariant(map));

        // assert
        REQUIRE(convertedAppStateInfo.activeTool() == ButtonInfo::PENCIL);
    }

    SECTION("Conversion with missing ActiveColour data") {
        // arrange
        QVariantMap map;
        map["OpenedFiles"] = QVariant(QStringList() << "file1.txt" << "file2.txt");
        map["ChosenTool"] = QVariant("BRUSH");
        AppStateInfo convertedAppStateInfo;

        // act
        convertedAppStateInfo.fromQVariant(QVariant(map));

        // assert
        REQUIRE(convertedAppStateInfo.activeColor() == QColor(0, 0, 0));
    }

    SECTION("Conversion with invalid ActiveColour data") {
        // arrange
        QVariantMap map;
        map["OpenedFiles"] = QVariant(QStringList() << "file1.txt" << "file2.txt");
        map["ChosenTool"] = QVariant("BRUSH");
        map["ActiveColour"] = QVariant(QVariantMap{
            {"Red", "invalid"},
            {"Green", "color"},
            {"Blue", "values"}
        });
        AppStateInfo convertedAppStateInfo;

        // act
        convertedAppStateInfo.fromQVariant(QVariant(map));

        // assert
        REQUIRE(convertedAppStateInfo.activeColor() == QColor(0, 0, 0));
    }
}
