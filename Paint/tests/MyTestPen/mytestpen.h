#ifndef MYTESTPEN_H
#define MYTESTPEN_H

#include "../../src/Pen/pen.h"


class MyTestPen
{
public:
    MyTestPen();

    int getBasePenStyle();
    int getBasePenCap();
    int getBasePenJoin();
    QPoint getLastPosition();
    QPoint getPosAfterStoppedDrawing();
    QPoint getPosAfterDraw(const QPoint point);
    QColor getLighterColor(const QColor old_color, const int factor);
};

#endif // MYTESTPEN_H
