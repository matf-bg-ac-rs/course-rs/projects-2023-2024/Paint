#include "mytestpen.h"


MyTestPen::MyTestPen() {}

int MyTestPen::getBasePenStyle() {
    Pen& pen = Pen::getInstance();

    return pen.base_pen.style();
}

int MyTestPen::getBasePenCap() {
    Pen& pen = Pen::getInstance();

    return pen.base_pen.capStyle();
}

int MyTestPen::getBasePenJoin() {
    Pen& pen = Pen::getInstance();

    return pen.base_pen.joinStyle();
}

QPoint MyTestPen::getLastPosition() {
    Pen& pen = Pen::getInstance();

    return pen.last_position;
}

QPoint MyTestPen::getPosAfterStoppedDrawing() {
    Pen& pen = Pen::getInstance();
    pen.stoppedDrawing();

    return pen.last_position;
}

QPoint MyTestPen::getPosAfterDraw(const QPoint point) {
    Pen& pen = Pen::getInstance();
    QPixmap pixmap(1000, 1000);

    pen.startDrawing(pixmap, point);
    pen.draw(point);
    QPoint last_pos = pen.last_position;
    pen.stoppedDrawing();

    return last_pos;
}

QColor MyTestPen::getLighterColor(const QColor old_color, const int factor) {
    Pen& pen = Pen::getInstance();

    QColor lighter_color = pen.lighterColor(old_color, factor);

    return lighter_color;
}
